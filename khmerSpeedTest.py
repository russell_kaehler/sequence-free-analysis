"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""
import time
import khmer

import commonFunctions
import processFastaAll

def read_file(file_path):
    tagFragDict = {}
    tagCounts = 0
    fragCounts = 0
    tag = ""
    frag = ""
    with open(file_path, "r") as myFile:
        for line in myFile:
            if( line[:1] == ">"):
                tag = line
                tagCounts += 1
            if( line[:1] != ">"):
                frag = line
                fragCounts += 1
            if( (frag != "") and (tag != "")):
                tagFragDict[tag] = frag
                tag = ""
                frag = ""
        if tagCounts == fragCounts:
            return tagFragDict.copy()

    return 5
    
def nu_read_file(file_path):
    tag_counts = 0
    frag_count = 0
    frags = []
    with open(file_path, "r") as myFile:
        for line in myFile:
            first_char = line[0]
            if( first_char == ">"):
                tag_counts += 1
            if( first_char != ">"):
                frags.append(line)
                frag_count += 1
    if tag_counts == frag_count:
        return frags[:]

    return 5


def mer_index_finder(my_string):
    my_string = my_string.lower()
    char_value = {}
    char_value["a"] = 0
    char_value["t"] = 1
    char_value["c"] = 2
    char_value["g"] = 3
    i = 0
    j = 0
    base_four_string = ""

    myStrLen = len(my_string)
    while(i < myStrLen):
        base_four_string += str(char_value[my_string[i]])
        i += 1

    index = int(base_four_string, 4)

    return index

def nu_mer_index_finder(my_string, mer_size):
    # my_string = my_string.lower()
    char_value = {}
    char_value["a"] = 0
    char_value["t"] = 1
    char_value["c"] = 2
    char_value["g"] = 3
    i = 0
    j = 0
    base_four_string = ""

    # myStrLen = len(my_string)
    while(i < mer_size):
        base_four_string += str(char_value[my_string[i]])
        i += 1

    index = int(base_four_string, 4)

    return index

def mer_count_khmer(mer_size, fragment, slidingSize):
    ktable = khmer.new_ktable(mer_size)
    ktable.consume(fragment)
    return ktable.copy()


def nu_mer_count(merSize, file_fragments, slidingSize):
    mer_counts = {}
    for fragment in file_fragments:
        j = 0
        max_j = len(fragment) - merSize
        # print fragment
        # max_j = 10000000
        while( j < max_j):
            mer_frag = fragment[j:j+merSize]
            mer_frag = mer_frag.lower()
            # print mer_frag
            if( "n" not in mer_frag):
                try:
                    mer_counts[mer_frag] += 1
                except:
                    mer_counts[mer_frag] = 1
                #myNSV[mer_index] += 1
                # myNSV[compliment_index] += 1
            j += slidingSize

    myNSV = [0] * (4**merSize)
    for mer in mer_counts.keys():
        mer_index = nu_mer_index_finder(mer, mer_size)
        myNSV[mer_index] = mer_counts[mer]

    return myNSV[:]


def mer_count_old_way(merSize, file_fragments, slidingSize):
    # keys = tagFragDict.keys()

    myNSV = [0] * (4**merSize)

    # for key in keys:
    #     myFrag = tagFragDict[key]
    for fragment in file_fragments:
        j = 0
        while( j < len(fragment) - merSize):
            mer_frag = fragment[j:j+merSize]
            mer_frag = mer_frag.lower()
            if( "n" not in mer_frag):
                mer_index =  mer_index_finder(mer_frag)
                # mer_compliment = make_complment_mer( mer_frag)
                # compliment_index = merIndexFinder(mer_compliment)
                myNSV[mer_index] += 1
                # myNSV[compliment_index] += 1
        
            j += slidingSize

    return myNSV[:]


def ini_read(file_list, mer_size):

    start_time = time.time()

    data_list = []
    for file_path in file_list:
        data_list.append(read_file(file_path))

    end_time = time.time()

    time_taken = end_time - start_time
    num_files = len(data_list)
    print "read %s files in %s sec" %(str(num_files), str(time_taken))
    return data_list[:]

def nu_read(file_list, mer_size):

    start_time = time.time()

    data_list = []
    for file_path in file_list:
        data_list.append(nu_read_file(file_path))

    end_time = time.time()

    time_taken = end_time - start_time
    num_files = len(data_list)
    print "read %s files in %s sec" %(str(num_files), str(time_taken))
    return data_list[:]




if __name__ == "__main__":
    mer_size = 12
    split_base = "/home/socrates/Test Splits/"
    fasta_files = commonFunctions.get_files_and_path(split_base)
    num_paths = len(fasta_files)
    # print "testing with old read method"
    # print "test is using %s paths" % (str(num_paths))
    # dict_method = ini_read(fasta_files, mer_size)
    # print "\n\n"
    # print "testing new read"
    list_method = nu_read(fasta_files, mer_size)

    start_nu = time.time()
    nu_nsv = nu_mer_count(mer_size, list_method[0], 1)
    end_nu = time.time()
    nu_time = end_nu - start_nu
    print "new count method took %s sec" % (nu_time)

    # start_old = time.time()
    # old_nsv = mer_count_old_way(mer_size, list_method[0], 1)
    # end_old = time.time()
    # old_time = end_old - start_old
    # print "old count method took %s sec" % (old_time)

    # for x in range(0,len(old_nsv)):
    #     if nu_nsv[x] != old_nsv[x]:
    #         print x
