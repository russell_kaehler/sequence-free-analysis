import random
import multiprocessing
import os

def removeExisitingData( onPath):
  eltsToRemove = getFiles(onPath)
  print eltsToRemove
  for elt in eltsToRemove:
    os.remove(onPath+elt)

  return 0


def getFiles( path):
  fileList = []
  for x in os.listdir(path):
    #fPath = path + x
    #print fPath
    fileList.append(x)
  return fileList

def make_random_fasta( passed):
  path = passed[0]
  numRecords = passed[1]
  n = 0 
  with open(path, "w") as myFile:
    for i in xrange(numRecords):
      tag_val = n 
      n += 1
      myFile.write( "> stuff about dna" + str(tag_val) + "\n")
      seq_size = random.randint(150,1000)
      my_seq = ""
      for j in xrange(seq_size):
        my_seq +=  random.choice("ATCG")
      myFile.write(my_seq + "\n")
  return 0

def main():
  pool = 10
  numRandom = 3
  numRecords = 10000000
  classes = ["Wolves", "Lions", "Bears"]

  outputPath = "/home/rkaehler/Random Fasta/"
  removeExisitingData(outputPath)
  items = []
  for a in classes:
    print a
    for i in xrange(numRandom):
      print "\t",i
      outputName = outputPath + a + "-" + str(i) +".fasta"
      print type(outputName),"\t",type(numRecords)
      print [outputName, numRecords]
      items.append([outputName, numRecords])
      
  myPool = multiprocessing.Pool(pool)
  myPool.map(make_random_fasta, items)
  return 1


if __name__ == "__main__":
  main()
