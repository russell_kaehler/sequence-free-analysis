#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student

This file is the naive bayes classifier
It should be able to run on any generalized numeric
data set with a class vector as the right hand most
column
"""


import commonFunctions
import metagenomicKNN


# naieve bayes should be added to the platform

# return a matrix from the data set such that all
# cols that aren't a class indicator we know the max/mix
# make it such that ret_data[col][0] is min
# ~lr ret_data[col][1] is max
# for all col in the data set

def get_max_min(data_set):
    "" " Find the max and min val for all col in the data set" ""
    max_min_matrix = []
    n_row = len(data_set)
    n_col = len(data_set[0]) - 1

    for col in xrange(n_col):
        col_min = float('inf')
        col_max = float('-inf')
        for row in xrange(n_row):
            ij_val = float(data_set[row][col])
            if ij_val > col_max:
                col_max = ij_val
            if ij_val < col_min:
                col_min = ij_val
        max_min_matrix.append([col_min, col_max])
    return max_min_matrix


def calculate_bin_sizes(max_min_matrix, n_bins):
    """ given the number of bins and the
    max min matrix of column information
    this function will return the appropriate width for the bins"""
    bin_sizes = []

    # do bin size calc by
    # binSizeForNDim = (nMax - nMin) / numBins

    for col in xrange(len(max_min_matrix)):
        bin_size = (max_min_matrix[col][1] - max_min_matrix[col][0]) \
            / n_bins
        bin_sizes.append(bin_size)

    return bin_sizes


def find_bin(value_to_bin, lower_bound, bin_size, n_bins):
    """ given a value and bin information select the correct
    bin for the value to be inserted into"""
    selected_bin = 0

    found_bin = False

    lhs = lower_bound
    rhs = lhs + bin_size
    while not found_bin:
        if lhs < value_to_bin and value_to_bin <= rhs:
            found_bin = True
        elif value_to_bin <= lhs and selected_bin == 0:
            found_bin = True
        elif value_to_bin >= n_bins * bin_size + lower_bound:
            found_bin = True
        else:
            lhs = rhs
            rhs += bin_size
            selected_bin += 1
    if selected_bin == n_bins:
        selected_bin -= 1

    return selected_bin


def bin_data(data_set, max_min_matrix, bin_sizes, n_bins, classes):
    """ for an entire data set correctly assign all records to
    the correct bins, return a bin matrix as a new data set"""
    nu_data = []
    n_cols = len(data_set[0]) - 1
    n_rows = len(data_set)

    for j in xrange(0, n_cols):
        bins = []
        bin_counter = 0
        while bin_counter < n_bins:
            bin_map = {}
            for class_opt in classes:
                bin_map[class_opt] = 0
            bins.append(bin_map.copy())
            bin_counter += 1

        for i in xrange(0, n_rows):
            ij_bin = find_bin(float(data_set[i][j]),
                              max_min_matrix[j][0], bin_sizes[j],
                              n_bins)

            bins[ij_bin][data_set[i][-1]] += 1
        nu_data.append(bins[:])

    return nu_data[:]


def count_classes_in_bins(binned_data):
    """ given the binned data and the number of bins
    find how many items for each class are in each bin"""
    bin_mapping = []
    n_col = len(binned_data[0]) - 1
    for row in xrange(len(binned_data)):
        row_class = binned_data[row][-1]
        for col in xrange(n_col):
            bin_mapping[col][row_class] += 1

    return False


def select_dims_old(data_in, n_dims):
    """ use a t-test to find the top n dims in a data set"""
    
    my_t_scores = metagenomicKNN.find_T_score(data_in[:])

    # get top ten dimensions
    the_best_dims = metagenomicKNN.findBestNDims(n_dims, my_t_scores[:])
    print "old method of getting best dims"
    print the_best_dims

    return the_best_dims


def select_dims(data_in, n_dims):
    """ use a t-test to find the top n dims in a data set"""

    my_t_scores = metagenomicKNN.find_T_score(data_in[:])

    the_best_dims = commonFunctions.find_best_n_dims_from_t_test_results( n_dims, my_t_scores[:])
    print "\"selected dims\":" + str(the_best_dims)

    return the_best_dims


def external_request(test_file, training_file, n_bins, class_list, m_est, n_dims):
    """ this is the function to call to run the classifier
    from an exernal file """
    
    training_data = commonFunctions.data_parser(training_file)
    test_data = commonFunctions.data_parser(test_file)

    training_data = commonFunctions.make_data_numeric(training_data[:])

    test_data = commonFunctions.make_data_numeric(test_data[:])

    if n_dims < (len(test_data[0]) - 1):
        use_dims = select_dims(training_data[:], n_dims)

        training_data = commonFunctions.pull_subset_of_data_keep_class_vect(use_dims[:], training_data[:])
        
        test_data = commonFunctions.pull_subset_of_data_keep_class_vect( use_dims[:], test_data[:])
    else:
        print "using all dims"

    # get max min for all dims
    max_min_values = get_max_min(training_data)

    bin_sizes = calculate_bin_sizes(max_min_values[:], n_bins)

    # make upper low bound for all dims
    # print bin_sizes
    # bin training data & bin testing

    binned_training_data = bin_data(training_data, max_min_values,
                                    bin_sizes, n_bins, class_list)

    final_votes = {}
    for row in xrange(0,len(test_data)):
        final_votes[test_data[row][-1]] = {}
        
    test_cols = len(test_data[0]) - 1
    for elt in xrange(len(test_data)):
        elt_bins = []
        for col in xrange(test_cols):
            col_bin = find_bin(float(test_data[elt][col]),
                               max_min_values[col][0], bin_sizes[col],
                               n_bins)
            elt_bins.append(col_bin)

        votes_map = {}

        for class_item in class_list:
            prob = 1.0
            for dim in xrange(test_cols):

                numerator = \
                    binned_training_data[dim][elt_bins[dim]][class_item] + 1
                numerator = float(numerator)

                denominator = \
                    sum(binned_training_data[dim][elt_bins[dim]].values()) \
                    + m_est
                denominator = float(denominator)

                prob *= numerator / denominator
                
            votes_map[class_item] = prob
        try:
            final_votes[test_data[elt][-1]][max(votes_map, key=votes_map.get)] += 1
        except:
            final_votes[test_data[elt][-1]][max(votes_map, key=votes_map.get)] = 1
    
    print "\"trail_results\":"+ str(final_votes)
    return final_votes.copy()


if __name__ == '__main__':

    # fruit test!

    TRAINING_FILE_MAIN = \
        '/Users/russkaehler/Machine Learning Spring ' + \
        '2012/KaehlerNaiveBayes/fruit.txt'
    TESTING_FILE_MAIN = \
        '/Users/russkaehler/Machine Learning Spring ' + \
        '2012/KaehlerNaiveBayes/testFruit.txt'
    CLASS_LIST_MAIN = ['1', '2', '3', '4']
    M_EST_MAIN = 0.00001
    external_request(TESTING_FILE_MAIN, TRAINING_FILE_MAIN,
                     4, CLASS_LIST_MAIN, M_EST_MAIN, 4)
