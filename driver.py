import datetime

import processFastaAll
import jGrassAvgAndRep
import jGrassFileMerger
import metagenomicKNN

#This will be the unifying driver for the pipeline 
#once this file is complete you should only have to modify any of the parameters here to run the rest of the system
if __name__ == "__main__":
	
	
	finalOutput = ""
	#knowledge of classes (list?)
	#list of file paths
	merSize = 4
	stepSize = 1
	#worker pool (necessary?)
	workers = 12
	
	currentTimeStr = str(datetime.datetime.now())
	print "Call has been made to run script"
	print "number of workers\t", workers
	print "mer size\t", merSize
	print "mer has step of\t", stepSize
	machineName = "rkaehler"
	
	#must have starting data set
	mainPath = "/home/" + machineName + "/Spring 2015 Bioinformatics Research"
	
	#inputFile = mainPath + "/Human/Human Gut NSV/8Mer/"
	#outputFile = mainPath + "/Human/Normal Gut/8Mer/"

	# sampleType = "Human"
	sampleType = "Johnson Grass Data"
	
	if sampleType == "Human":
		sampleDir = "/" + sampleType
		#input fasta is used by the process fasta script
		inputFasta = mainPath + sampleDir + "/Fasta Files/"
		#this is used as the output for process fasta output
		#also used as the input for avg and rep
		nsvLocation = mainPath + sampleDir + "/Human Gut NSV/" + str(merSize) + "Mer/"
		#output for avg and rep
		#input for file merger
		normalizedNSV = mainPath + sampleDir + "/Normal Gut/" + str(merSize) + "Mer/"
		#output for merger
		#input for metagenomic processing
		finalOutput = mainPath + "/normalized" + sampleType + "GutWithClassVect" + str(merSize) + "-Mers at time" + currentTimeStr + ".csv"
		gutClasses = ["Healthy/", "Crohns/", "UC/"]
	
		altGutClasses = ["Healthy", "Crohns", "UC"]

		classNames = altGutClasses[:]
		classMap = {} #needs to be zip map of names -> numbers
		classMap["hea"] = 0
		classMap["cro"] = 1
		classMap["ucn"] = 2
		#call initial fasta to nsv all file
		print "Calling fasta process script"
		processFastaAll.externalRequest( inputFasta, nsvLocation, workers, merSize, stepSize)
		
		#average samples and produce a rep
		print "Calling avg maker"
		jGrassAvgAndRep.streamlineRequest( nsvLocation, normalizedNSV, classNames)
		
		#merge all samples 
		#TODO: merge all reps
		print "Calling file merger"
		jGrassFileMerger.externalRequest(normalizedNSV, finalOutput, merSize, classMap)
		
		#call ML package (initially KNN)
		print "Calling KNN ML script"
		metagenomicKNN.externalRequest(finalOutput)

	# Looking at grass data
	if sampleType == "Johnson Grass Data":
		sampleDir = "/" + sampleType
		#input fasta is used by the process fasta script
		inputFasta = mainPath + sampleDir + "/Johnson Grass Fasta/"
		#this is used as the output for process fasta output
		#also used as the input for avg and rep
		nsvLocation = mainPath + sampleDir + "/Johnson Grass NSV/" + str(merSize) + "Mer/"
		#output for avg and rep
		#input for file merger
		normalizedNSV = mainPath + sampleDir + "/Normal J Grass/" + str(merSize) + "Mer/"
		#output for merger
		#input for metagenomic processing
		finalOutput = mainPath + "/normalized" + sampleType + "JGrassWithClassVectAndOverlap" +str(stepSize) +"and"+ str(merSize) + "-Mers at time" + currentTimeStr + ".csv"

		jGrassClasses = ["Transition", "Native", "Invaded"]

		classNames = jGrassClasses[:]
		classMap = {} #needs to be zip map of names -> numbers
		classMap["nat"] = 0
		classMap["tra"] = 1
		classMap["inv"] = 2

		#call initial fasta to nsv all file
		print "Calling fasta process script"
		processFastaAll.externalRequest( inputFasta, nsvLocation, workers, merSize, stepSize)
		
		#average samples and produce a rep
		print "Calling avg maker"
		jGrassAvgAndRep.streamlineRequest( nsvLocation, normalizedNSV, classNames)
		
		#merge all samples 
		#TODO: merge all reps
		print "Calling file merger"
		jGrassFileMerger.externalRequest(normalizedNSV, finalOutput, merSize, classMap)
		
		#call ML package (initially KNN)
		print "Calling KNN ML script"
		metagenomicKNN.externalRequest(finalOutput)

		#return 0
