"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""

import itertools
from os import listdir
import datetime

import commonFunctions


def gatherLines(dest, classNum):
	retList = []
	with open(dest, 'r') as myFile:
		for line in myFile:
			myLine = [x.strip() for x in line.split(',')]
			myLine.append(classNum)
			retList.append(myLine)
			# print len(myLine)
	return retList
			

def splitOnComma(myStr):
	floatArray = []
	tmpString = ""
	iniFlag = 0
	for i in range(0,len(myStr)):
		#print myStr[i]
		myVal = myStr[i]
		if ((myVal != ",") and (myVal != "\n")):
			tmpString += myVal
		if ((myVal == ",") and (tmpString != "")):
			myFloat = float(tmpString)
			floatArray.append(myFloat)
			if (iniFlag == 0):
				# print tmpString
				iniFlag = 1
			tmpString = ""
	# print floatArray[0]
	# print len(floatArray)
	return floatArray


def makeHeaderForFile(myOutFile, merSize):
	bases = ["a" , "t", "c", "g"]
	merOptions = itertools.product(bases, repeat = merSize)
	with open(myOutFile, "w") as myOut:
		for i in merOptions:
			merStr = ""
			for n in i:
				merStr += n
			#if merStr != ("g" * merSize):
			myOut.write(merStr + ",")
			#else:
				#myOut.write(merStr + "\n")
		myOut.write("Class\n")


def headerStrictlyNumeric(myOutFile, merSize):
	with open(myOutFile, "w") as myOut:
		for i in range(0, 4**merSize):
			myOut.write(str(i) + ",")
		myOut.write("Class\n")


def externalRequest_by_filename( inPath, outPath, outFileName, kSize):
	# print "Hit external request of file merger"

	commonFunctions.clearOutputPath(outPath)

	fileList = commonFunctions.getFiles(inPath)

	finalLineArray = []

	for x in range(0,len(fileList)):
		myFile = fileList[x]
		searchString = str(kSize)+"mer"
		recClass = myFile[:3].lower()
		
		# print recClass
		# print classDict[recClass]
		
		if "rep" not in myFile.lower():
			# print myFile
			fullFile = inPath + myFile
			linesArray = gatherLines(fullFile, myFile)
			# print len(linesArray)
			# print linesArray[0][0]
			for n in range(0,len(linesArray)):
				finalLineArray.append(linesArray[n])
			#cnt += 1

	with open(outFileName, 'a') as outFile:
		for q in range(0,len(finalLineArray)):
			tempLine = finalLineArray[q]
			tempLen = len(tempLine)
			for r in range(0, tempLen):
				val = str(tempLine[r])
				if (r != tempLen-1):
					outFile.write( val + ",")
				else:
					outFile.write( val + "\n")


def externalRequest_by_class( inPath, outPath, outFileName, kSize, classDict):
	# print "Hit external request of file merger"

	commonFunctions.clearOutputPath(outPath)
	#currentTimeStr = str(datetime.datetime.now())
	#Type of subject eg human and class info need to be set in driver 
	#(most of this string can be developed by the driver)
	
	#iniFile = pathMain + "/Human/Normal Gut/"+str(kSize)+"Mer/"
	
	#TODO make output have timestamp
	#see note above about string formatting being done in the driver

	#TODO this needs to be a passed var
	#myOutput = pathMain + "/normalizedHumanGutWithClassVect" +str(kSize)+"-Mers at time"+currentTimeStr+".csv"

	fileList = commonFunctions.getFiles(inPath)

	finalLineArray = []

	for x in range(0,len(fileList)):
		myFile = fileList[x]
		searchString = str(kSize)+"mer"
		recClass = myFile[:3].lower()
		
		# print recClass
		# print classDict[recClass]
		
		if "rep" not in myFile.lower():
			# print myFile
			fullFile = inPath + myFile
			linesArray = gatherLines(fullFile, classDict[recClass])
			# print len(linesArray)
			# print linesArray[0][0]
			for n in range(0,len(linesArray)):
				finalLineArray.append(linesArray[n])
			#cnt += 1

	with open(outFileName, 'a') as outFile:
		for q in range(0,len(finalLineArray)):
			tempLine = finalLineArray[q]
			tempLen = len(tempLine)
			for r in range(0, tempLen):
				val = str(tempLine[r])
				if (r != tempLen-1):
					outFile.write( val + ",")
				else:
					outFile.write( val + "\n")


if __name__ == "__main__":
	mainPath = "/home/socrates/Spring 2015 Bioinformatics Research"
	
	currentTimeStr = str(datetime.datetime.now())

	kSize = 8

	print currentTimeStr

	#classNames = ["transition", "native", "invaded"]

	iniFile = mainPath + "/Human/Normal Gut/"+str(kSize)+"Mer/"
	#TODO make output have timestamp
	myOutput = mainPath + "/normalizedHumanGutWithClassVect" +str(kSize)+"-Mers at time"+currentTimeStr+".csv"


	fileList = commonFunctions.getFiles(iniFile)

	finalLineArray = []

	#TODO polish clas map later
	#mapping of classes must be known 
	classVals ={}
	classVals["ucn"] = 0
	classVals["hea"] = 1
	classVals["cro"] = 2
	
	
	#TODO change count to a mapping of classes
	#must be str to rep class, fixed numeric map, or always have key generated with file
	#cnt = 0
	for x in range(0,len(fileList)):
		myFile = fileList[x]
		searchString = str(kSize)+"mer"
		recClass = myFile[:3].lower()
		
		# print recClass
		# print classVals[recClass]
		
		if "rep" not in myFile.lower():
			# print myFile
			fullFile = iniFile + myFile
			linesArray = gatherLines(fullFile, classVals[recClass])
			# print len(linesArray)
			# print linesArray[0][0]
			for n in range(0,len(linesArray)):
				finalLineArray.append(linesArray[n])
			#cnt += 1
	
	with open(myOutput, 'a') as outFile:
		for q in range(0,len(finalLineArray)):
			tempLine = finalLineArray[q]
			tempLen = len(tempLine)
			for r in range(0, tempLen):
				val = str(tempLine[r])
				if (r != tempLen-1):
					outFile.write( val + ",")
				else:
					outFile.write( val + "\n")

	

