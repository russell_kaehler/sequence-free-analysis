#Russell Kaehler
#April 2014
#takes in batches of fastq files and translates all of them to fasta

from os import listdir

def getFiles( path):
	fileList = []
	for x in listdir(path):
		#fPath = path + x
		#print fPath
		fileList.append(x)
	return fileList


def processFasta(inputPath, fileName, outputPath, outName):
	fastqPath = inputPath + fileName

	plStartChar = ''
	pLine = ''
	seqMap = {}
	orgName = ''
	foundNewOrg = 0
	with open(fastqPath, 'r') as myFile:
		for line in myFile:
			check = 0
			myLine = line.replace('\r\n', '').strip()
			if myLine[0:1] == "@" and plStartChar != "+":
				orgName = myLine
				foundNewOrg = 1
				check = 1
			#print len(pLine)
			if plStartChar == "@" and foundNewOrg == 1 and check == 0: 
				seqMap[orgName] = myLine
				foundNewOrg = 0
			plStartChar = myLine[0:1]
			pLine = myLine

	myKeys = seqMap.keys()
	#for key in myKeys:
	#	print key[1:]

	print len(myKeys)

	fastaFile = outputPath + outName

	mylen = int( len(myKeys))
	print mylen

	with open(fastaFile, 'a') as outFile:
		for key in myKeys:
			#print len(seqMap.keys())
			if( len(seqMap[key]) >= 100):
				outFile.write(">" + key[1:] + ";\n" + seqMap[key] + "\n")


basePath = "/home/brain/Spring 2015 Bioinformatics Research"
iniPath = basePath + "/FastQ/"
outPath = basePath + "/Johnson Grass Fasta/"

"""
Key to the metagenomic johnsongrass data
DNA_N1	Index13	AGTCAA 
DNA_N2	Index14	AGTTCC 
DNA_N3	Index15	ATGTCA 
DNA_N4	Index16	CCGTCC 
DNA_T1	Index18	GTCCGC 
DNA_T2	Index19	GTGAAA 
DNA_T3	Index20	GTGGCC 
DNA_T4	Index21	GTTTCG 
DNA_I1	Index22	CGTACG 
DNA_I2	Index23	GAGTGG 
DNA_I3	Index25	ACTGAT 
DNA_I4	Index27	ATTCCT 
"""

myFiles = getFiles(iniPath)
for i in myFiles:
	#print i
	iOut = ""
	if ("index13" in i.lower()) or ("index14" in i.lower()) or ("index15" in i.lower()) or ("index16" in i.lower()):
		iOut = "Native_" + i[:-3] + ".fasta"
		print "found native\t" + i
	if ("index18" in i.lower()) or ("index19" in i.lower()) or ("index20" in i.lower()) or ("index21" in i.lower()):	
		iOut = "Transition_" + i[:-3] + ".fasta"
		print "found transition\t" + i
	if ("index22" in i.lower()) or ("index23" in i.lower()) or ("index25" in i.lower()) or ("index27" in i.lower()):	
		iOut = "Invaded_" + i[:-3] + ".fasta"
		print "found invaded\t" + i
	if iOut != "":
		processFasta(iniPath, i, outPath, iOut)
		
		
	
