"""
Copyright (c)  Russell Kaehler 2016
University of Montana
Computer Science Grad Student
"""

import numpy as np
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2


import metagenomicKNN
import commonFunctions

# import warnings

# warnings.simplefilter("error")

def makeDistList( trainingDataIn, testRecord):
	distAB = {}

	for alt in range(0, len(trainingDataIn)):
		dist = metagenomicKNN.find_Manhattan(testRecord[:-1], trainingDataIn[alt][:-1])
		distAB[alt] =dist
	return distAB.copy()


def pull_testing_files( testingPath):

	test_file = commonFunctions.getFiles( testingPath)

	testingMap = {}

	for item in test_file:
		itemTestData = metagenomicKNN.data_parser( testingPath + item)
		testingMap[item] = itemTestData[:]

	return testingMap


def select_dims_old(data_in, n_dims):
	""" use a t-test to find the top n dims in a data set"""
	# print "getting t-test scores"
	my_t_scores = metagenomicKNN.find_T_score(data_in[:])
	# print "found t-test scores"
	
	# print "\n"
	the_best_dims = commonFunctions.find_best_n_dims_from_t_test_results( n_dims, my_t_scores[:])
	print "\"selected dims\":" + str(the_best_dims)
	# print the_best_dims

	return the_best_dims


def select_dims(data_in, n_dims):
    """ use a t-test to find the top n dims in a data set"""

    my_array = np.array(data_in,dtype=float)
    data_part = my_array[:,:-1]
    # print data_part
    class_part = my_array[:,-1]
    # print class_part
    X_res = SelectKBest(chi2, k=n_dims).fit(data_part, class_part)
    # print X_res.get_support()
    the_best_dims = np.where(X_res.get_support().tolist())[0].tolist()
    print "\"selected dims\":" + str(the_best_dims)

    return the_best_dims



def external_request( main_path, votingSize, n_dims):
	""" call this function to run the driver """

	final_train = main_path + "/Final Train/"
	final_test = main_path + "/Final Test/"

	#input for metagenomic processing
	final_train_output = final_train + "trainingNormalized.csv"
	final_test_output = final_test + "testingNormalized.csv"

	training_data = commonFunctions.data_parser(final_train_output)
	test_data = commonFunctions.data_parser(final_test_output)

	training_data = commonFunctions.make_data_numeric(training_data[:])
	test_data = commonFunctions.make_data_numeric(test_data[:])

	# theTen = select_dims(training_data, n_dims)
    
	if n_dims < (len(test_data[0]) - 1):
		use_dims = select_dims(training_data[:], n_dims)

		training_data = commonFunctions.pull_subset_of_data_keep_class_vect(use_dims[:], training_data[:])

		test_data = commonFunctions.pull_subset_of_data_keep_class_vect( use_dims[:], test_data[:])
	else:
		print "using all dims"
	# print "\n\n\n"

	winners = []
	final_results = {}
	for testRecord in test_data:

		distAB = makeDistList(training_data, testRecord)
		tempDict = {}
		for j in distAB.keys():

			ijDist = distAB[j]

			if len(tempDict.keys()) < votingSize:
				tempDict[j] = ijDist
			else:
				maxValue = max(tempDict.values())
				#print maxValue
				valueRow = -1
				for key in tempDict.keys():
					if maxValue == tempDict[key]:
						valueRow = key
			  
				if (ijDist < maxValue) and (valueRow != -1):
					del(tempDict[valueRow])
					tempDict[j] = ijDist
		winners.append(tempDict)

	votes = {}
	for i in range(0, len(winners)):
		rowDict = winners[i]

		rowKeys = rowDict.keys()
		itemClass = test_data[i][-1]
		if itemClass not in final_results.keys():
			final_results[itemClass] = {}

		for x in range( 0, len(rowKeys)):
			rowClass = training_data[rowKeys[x]][-1]
			if rowClass not in votes.keys():
				votes[rowClass] = 1
			else:
				votes[rowClass] += 1
			try:
				final_results[itemClass][rowClass] += 1
			except:
				final_results[itemClass][rowClass] = 1
			# print itemClass,"\t",rowClass
	# print votes	
	# print final_results	
	print "\"trail_results\":"+ str(final_results)
	return final_results


if __name__ == "__main__":
  print "Regret nothing"