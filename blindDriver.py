import datetime

import processFastaAll
import jGrassAvgAndRep
import make_avg
import jGrassFileMerger
import nuKNN
import dataSlicer


if __name__ == "__main__":

    finalOutput = ""
    # knowledge of classes (list?)
    # list of file paths
    merSize = 4
    stepSize = 1
    # worker pool (necessary?)

    fragLimit = 400000
    parts = 10
    # workers should be 3x number of parts, 3 classes each with n parts
    workers = 3 * parts

    n_dims = 20

    currentTimeStr = str(datetime.datetime.now())
    print "Call has been made to run script"
    print "number of workers\t", workers
    print "mer size\t", merSize
    print "mer has step of\t", stepSize
    machineName = "rkaehler"

    # must have starting data set
    mainPath = "/home/" + machineName

    # sampleType = "JGrass"

    # sampleDir = "/" + sampleType

    # rawTrain = mainPath + "/Fasta Train/"

    # rawTest = mainPath + "/Fasta Test/"

    rawTrain = mainPath + "/JGrass Train/"

    rawTest = mainPath + "/JGrass Test/"

    limitedTrain = mainPath + "/Limited Train/"

    limitedTest = mainPath + "/Limited Test/"

    # input fasta is used by the process fasta script
    inputTrain = mainPath + "/Train Splits/"
    inputTest = mainPath + "/Test Splits/"
    # this is used as the output for process fasta output
    # also used as the input for avg and rep
    nsvTrain = mainPath + "/NSV Train/"
    nsvTest = mainPath + "/NSV Test/"
    # output for avg and rep
    # input for file merger
    normalizedTrain = mainPath + "/Normalized Split/"

    normalizedTest = mainPath + "/Normalized Two/"

    finalTrain = mainPath + "/Final Train/"
    finalTest = mainPath + "/Final Test/"

    # input for metagenomic processing
    finalTrainOutput = finalTrain + "trainingNormalized.csv"

    finalTestOutput = finalTest + "testingNormalized.csv"

    # Classes = ["102", "103", "943"]
    jGrassClasses = ["Transition", "Native", "Invaded"]

    # classNames = Classes[:]
    classNames = jGrassClasses[:]

    classMap = {}  # needs to be zip map of names -> numbers
    classMap["nat"] = 0
    classMap["inv"] = 1
    classMap["tra"] = 2

    # testNames = ["nat", "inv", "tra"]
    testNames = ["Transition", "Native", "Invaded"]

    dataSlicer.main(rawTrain, limitedTrain, inputTrain,
                    classNames, fragLimit, parts)

    dataSlicer.main(rawTest, limitedTest, inputTest,
                    testNames, fragLimit, parts)

    # #call initial fasta to nsv all file
    # print "Calling fasta process script"
    processFastaAll.externalRequest(
        inputTrain, nsvTrain, workers, merSize, stepSize)

    # #call initial fasta to nsv all file
    # print "Calling fasta process script"
    processFastaAll.externalRequest(
        inputTest, nsvTest, workers, merSize, stepSize)

    # #average samples and produce a rep
    # print "Calling avg maker - for training data"
    # make_avg.external_request_test( nsvTrain, normalizedTrain, "training")
    # changing class map to class names
    jGrassAvgAndRep.streamlineRequest(nsvTrain, normalizedTrain, classNames)

    jGrassFileMerger.externalRequest_by_class(
        normalizedTrain, finalTrain, finalTrainOutput, merSize, classMap)

    jGrassAvgAndRep.streamlineRequest(nsvTest, normalizedTest, testNames)

    jGrassFileMerger.externalRequest_by_filename(
        normalizedTest, finalTest, finalTestOutput, merSize)

    # print "Calling avg maker - for test data"
    # make_avg.external_request_test( nsvTest, normalizedTest, "test")

    # call ML package (initially KNN)

    # print "Calling KNN ML script"
    # nuKNN.external_request( finalTrain, finalTest, 1, n_dims)
