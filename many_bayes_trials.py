"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""
import sys
import gc
import bayes_crossfold
import multi_ml_crossfold

def main():
    for i in xrange(25):
        #Run NB
        # run_res = bayes_crossfold.run_folds(read_path="/home/socrates", write_path="/home/socrates", mer_size=5, n_dims=10, make_log=0)
        #Run KNN
        run_res = multi_ml_crossfold.run_folds(read_path="/home/socrates", write_path="/home/socrates", mer_size=5, n_dims=10, make_log=0)

        collected = gc.collect()
        print "Garbage collector: collected %d objects." % (collected)
        if run_res != 0:
            sys.exit(3)

if __name__ == "__main__":
    main()