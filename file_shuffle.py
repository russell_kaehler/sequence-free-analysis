"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""

import commonFunctions
import itertools
import random



def mix_files(data_start_path, class_options):
	# get files
	ini_file_list = commonFunctions.getFiles(data_start_path)
	split_classes = {}
	# orders = itertools.permutations(ini_file_list)
	# organize sort
	for item in ini_file_list:
		for opt in class_options:
			if opt in item:
				try:
					split_classes[opt].append(item)
				except:
					split_classes[opt] = [item]
	# files will be a set of permeutations
	# do count of lines
	rand_orders = {}
	for opt in class_options:
		print opt
		rand_orders[opt] = []
		orders = itertools.permutations(split_classes[opt])
		for x in orders:
			rand_orders[opt].append(list(x))

	test_files = []
	training_files = []
	for opt in class_options:
		list_len = len(rand_orders[opt])

		for index in xrange(list_len):
			arangement = rand_orders[opt][index]
			# print arangement
			test_half = arangement[:len(arangement) / 2]
			train_half = arangement[len(arangement) / 2:]
			try:
				for elt in test_half:
					test_files[index].append(elt)
				for alt in train_half:
					training_files[index].append(alt)
			except:
				test_files.append([])
				training_files.append([])
				for elt in test_half:
					test_files[index].append(elt)
				for alt in train_half:
					training_files[index].append(alt)

	return test_files, training_files


if __name__ == "__main__":
	johnson_grass_data_path = "/home/rkaehler/Johnson Grass Fasta/"
	j_grass_classes = ["Transition", "Native", "Invaded"]
	mix_files(johnson_grass_data_path, j_grass_classes)
