import datetime

import processFastaAll
import jGrassAvgAndRep
import jGrassFileMerger
import metagenomicKNN


if __name__ == "__main__":
  
  
  finalOutput = ""
  #knowledge of classes (list?)
  #list of file paths
  merSize = 4
  stepSize = 1
  #worker pool (necessary?)
  workers = 30
  
  currentTimeStr = str(datetime.datetime.now())
  print "Call has been made to run script"
  print "number of workers\t", workers
  print "mer size\t", merSize
  print "mer has step of\t", stepSize
  machineName = "rkaehler"
  
  #must have starting data set
  mainPath = "/home/" + machineName
  
  #inputFile = mainPath + "/Human/Human Gut NSV/8Mer/"
  #outputFile = mainPath + "/Human/Normal Gut/8Mer/"

  # sampleType = "Human"
  
  # sampleType = "Johnson Grass Data"

  # sampleType = "Random Fasta"

  sampleType = "Saliva"

  sampleDir = "/" + sampleType
  #input fasta is used by the process fasta script
  inputFasta = mainPath + "/Temp Splits/"
  #this is used as the output for process fasta output
  #also used as the input for avg and rep
  nsvLocation = mainPath + "/Split NSV/"
  #output for avg and rep
  #input for file merger
  normalizedNSV = mainPath + "/Normalized Split/"

  finalPath = mainPath + "/Final Split/"

  if sampleType == "Human":

    #input for metagenomic processing
    finalOutput = finalPath + sampleType + "GutWithClassVect" + str(merSize) + "-Mers at time" + currentTimeStr + ".csv"
    # gutClasses = ["Healthy/", "Crohns/", "UC/"]
  
    altGutClasses = ["Healthy", "Crohns", "UC"]

    classNames = altGutClasses[:]
    classMap = {} #needs to be zip map of names -> numbers
    classMap["hea"] = 0
    classMap["cro"] = 1
    classMap["ucn"] = 2
    #call initial fasta to nsv all file
    print "Calling fasta process script"
    processFastaAll.externalRequest( inputFasta, nsvLocation, workers, merSize, stepSize)
    
    #average samples and produce a rep
    print "Calling avg maker"
    jGrassAvgAndRep.streamlineRequest( nsvLocation, normalizedNSV, classNames)
    
    #merge all samples 
    #TODO: merge all reps
    print "Calling file merger"
    jGrassFileMerger.externalRequest_by_class(normalizedNSV, finalPath, finalOutput, merSize, classMap)
    
    #call ML package (initially KNN)
    print "Calling KNN ML script"
    metagenomicKNN.externalRequest(finalOutput)

  # Looking at grass data


  # sampleType = "Johnson Grass Data"
  if sampleType == "Johnson Grass Data":

    #input for metagenomic processing
    finalOutput = finalPath + sampleType + "JGrassWithClassVectAndOverlap" +str(stepSize) +"and"+ str(merSize) + "-Mers at time" + currentTimeStr + ".csv"

    jGrassClasses = ["Transition", "Native", "Invaded"]

    classNames = jGrassClasses[:]
    classMap = {} #needs to be zip map of names -> numbers
    classMap["nat"] = 0
    classMap["tra"] = 1
    classMap["inv"] = 2

    #call initial fasta to nsv all file
    print "Calling fasta process script"
    processFastaAll.externalRequest( inputFasta, nsvLocation, workers, merSize, stepSize)
    
    #average samples and produce a rep
    print "Calling avg maker"
    jGrassAvgAndRep.streamlineRequest( nsvLocation, normalizedNSV, classNames)
    
    #merge all samples 
    #TODO: merge all reps
    print "Calling file merger"
    jGrassFileMerger.externalRequest_by_class(normalizedNSV, finalPath, finalOutput, merSize, classMap)
    
    #call ML package (initially KNN)
    print "Calling KNN ML script"
    metagenomicKNN.externalRequest(finalOutput)

  # sampleType = "Random Fasta"
  if sampleType == "Random Fasta":
    # inputFasta = mainPath + "/Random Fasta/"
    #input for metagenomic processing
    finalOutput = finalPath + sampleType + "RandomJunk" +str(stepSize) +"and"+ str(merSize) + "-Mers at time" + currentTimeStr + ".csv"

    Classes = ["Wolves", "Lions", "Bears"]

    classNames = Classes[:]
    classMap = {} #needs to be zip map of names -> numbers
    classMap["wol"] = 0
    classMap["lio"] = 1
    classMap["bea"] = 2

    #call initial fasta to nsv all file
    print "Calling fasta process script"
    processFastaAll.externalRequest( inputFasta, nsvLocation, workers, merSize, stepSize)
    
    #average samples and produce a rep
    print "Calling avg maker"
    jGrassAvgAndRep.streamlineRequest( nsvLocation, normalizedNSV, classNames)
    
    #merge all samples 
    #TODO: merge all reps
    print "Calling file merger"
    jGrassFileMerger.externalRequest_by_class(normalizedNSV, finalPath, finalOutput, merSize, classMap)
    
    #call ML package (initially KNN)
    print "Calling KNN ML script"
    metagenomicKNN.externalRequest(finalOutput)

  if sampleType == "Saliva":
    # inputFasta = mainPath + "/Random Fasta/"
    #input for metagenomic processing
    finalOutput = finalPath + sampleType + "RandomJunk" +str(stepSize) +"and"+ str(merSize) + "-Mers at time" + currentTimeStr + ".csv"

    Classes = ["102", "103", "943"]

    classNames = Classes[:]
    classMap = {} #needs to be zip map of names -> numbers
    classMap["102"] = 0
    classMap["103"] = 1
    classMap["943"] = 2

    #call initial fasta to nsv all file
    print "Calling fasta process script"
    processFastaAll.externalRequest( inputFasta, nsvLocation, workers, merSize, stepSize)
    
    #average samples and produce a rep
    print "Calling avg maker"
    jGrassAvgAndRep.streamlineRequest( nsvLocation, normalizedNSV, classNames)
    
    #merge all samples 
    #TODO: merge all reps
    print "Calling file merger"
    jGrassFileMerger.externalRequest_by_class(normalizedNSV, finalPath, finalOutput, merSize, classMap)
    
    #call ML package (initially KNN)
    print "Calling KNN ML script"
    metagenomicKNN.externalRequest(finalOutput)


































