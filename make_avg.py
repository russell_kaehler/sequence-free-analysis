import sys
import multiprocessing
import itertools
from os import listdir

import jGrassAvgAndRep
import commonFunctions



def external_request_train(inputFile, outputFile, classNames):
  print "Hit new request function"

  commonFunctions.clearOutputPath(outputFile)
  myStartingFiles = getFiles(inputFile)

  itemDict = {}
  for q in myStartingFiles:
    for k in classNames:
      if k in q:
        try:
          itemDict[k].append(q)
        except:
          itemDict[k] = [q]

  for case in classNames:
    dest = outputFile + case + "Normal.csv"
    repDest = outputFile + case + "NormalRep.csv"
    normalizedLines = []
    print inputFile,"\t",classNames
    print itemDict
    for elt in itemDict[case]:
      fullPath = inputFile + elt
      with open(fullPath, "r") as myFile:
        for line in myFile:
          iniLine = [int(x.strip()) for x in line.split(",")]
          lineNormalized = normalizeBySize(iniLine)
          normalizedLines.append(lineNormalized)
    print "Writing set to file"
    writeLineToFile( dest, normalizedLines)
    #build a rep(avg) file
    print "Making Class Rep"
    myRep = makeARep(normalizedLines)   

    writeRepToFile( repDest, myRep)


  return 0




def external_request_test( inputFile, outputFile, setName):
  print "Hit new request function"

  commonFunctions.clearOutputPath( outputFile)
  myStartingFiles = commonFunctions.getFiles( inputFile)

  dest = outputFile + setName + "Normalized.csv"

  normalizedLines = []

  for case in myStartingFiles:
    
    fullPath = inputFile + case
    with open( fullPath, "r") as myFile:
      for line in myFile:
        iniLine = [ int(x.strip()) for x in line.split(",")]
        lineNormalized = jGrassAvgAndRep.normalizeBySize(iniLine)
        lineNormalized.append( case)
        # print "length of normalized line\t", len(lineNormalized)
        # print lineNormalized
        # print "\n"
        normalizedLines.append( lineNormalized)
    print "Writing set to file"
  jGrassAvgAndRep.writeLineToFile( dest, normalizedLines)
  #build a rep(avg) file
  # print "Making Class Rep"
  # myRep = makeARep( normalizedLines)   

  # writeRepToFile( repDest, myRep)

  return 0


if __name__ == "__main__":
  #set dirs
  print "Run. . . Run. . . Leave your children and the weak and run. . ."