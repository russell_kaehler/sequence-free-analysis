"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""
import matplotlib.pyplot as plt
import math 

import commonFunctions
import metagenomicKNN


def gather_and_organize_files(path, class_options):
    my_files = commonFunctions.getFiles(path)
    files_by_class = {}
    for item in my_files:
        for option in class_options:
            if option in item:
                try:
                    files_by_class[option].append(item)
                except:
                    files_by_class[option] = [item]
    return files_by_class


def make_data_matrix_from_dict(read_path, incoming_dict):
    nu_matrix = []
    dict_classes = incoming_dict.keys()

    for index in xrange(len(dict_classes)):
        current_class = dict_classes[index]
        print current_class, "\t", index
        for item in incoming_dict[current_class]:
            # print "\t",read_path
            # print "\t", item

            numeric_data = commonFunctions.read_file_into_numeric_list(
                read_path + item)
            numeric_data.append(index)
            nu_matrix.append(numeric_data[:])
    return nu_matrix[:]


def select_dims(data_in, n_dims):
    """ use a t-test to find the top n dims in a data set"""
    print 'getting t-test scores'
    my_t_scores = metagenomicKNN.find_T_score(data_in)
    print 'found t-test scores'

    the_best_dims = metagenomicKNN.findBestNDims(n_dims, my_t_scores[:])

    return the_best_dims


def make_bar_char_from_data_matrix_and_isolate_dimensions(path, class_options, path_to_save_to):

    colors = ["red", "blue", "pink"]

    commonFunctions.clearOutputPath(path_to_save_to)
    files_by_class = gather_and_organize_files(path, class_options)

    data_matrix = make_data_matrix_from_dict(path, files_by_class)

    dim_limit = 5
    use_dims = select_dims(data_matrix, dim_limit)

    selected_data = commonFunctions.pull_subset_of_data_keep_class_vect(
        use_dims, data_matrix)

    # for option in xrange(len(class_options)):
    # read each file
    # num_files_in_option = len(files_by_class[class_options[option]])
    data_dict = {}
    for pos in selected_data:
        try:
            data_dict[pos[-1]].append(pos)
        except:
            data_dict[pos[-1]] = [pos]

    for class_set in data_dict.keys():
        f, ax_array = plt.subplots(
            len(data_dict[class_set]), sharex=True, sharey=True) #, figsize=(10, 10)
        for index in xrange(len(data_dict[class_set])):
            # numeric_data = commonFunctions.read_file_into_numeric_list(
            #     path + files_by_class[class_options[option]][index])
            # print files_by_class[class_options[option]][index]
            # print "\n"
            # print numeric_data
            # print "\n\n\n"

            # item_extension_location = files_by_class[
            #     class_options[option]][index].rfind(".")
            # item_no_extension = files_by_class[class_options[option]][
            #     index][:item_extension_location]

            plt.xlim(0, dim_limit)
            # plt.subplot()
            print data_dict[class_set][index][-1]
            c_val = int(data_dict[class_set][index][-1])
            print data_dict[class_set][index][:-1]
            log_vals = [math.log(x) for x in data_dict[class_set][index][:-1]]
            ax_array[index].bar(list(xrange(dim_limit)), log_vals, color=colors[c_val])
            # plt.tight_layout()
        plt.savefig(path_to_save_to + class_options[class_set] + "-small_data_with-" + str(dim_limit) +"-dims.pdf")

    return 0


def make_historgram_from_raw_files(path, class_options, path_to_save_to):
    commonFunctions.clearOutputPath(path_to_save_to)
    files_by_class = gather_and_organize_files(path, class_options)
    colors = ["red", "blue", "pink"]
    for option in xrange(len(class_options)):
        # read each file
        num_files_in_option = len(files_by_class[class_options[option]])
        f, ax_array = plt.subplots(
            num_files_in_option, sharex=True, sharey=True, figsize=(10, 10))
        for index in xrange(num_files_in_option):
            numeric_data = commonFunctions.read_file_into_numeric_list(
                path + files_by_class[class_options[option]][index])
            print files_by_class[class_options[option]][index]
            print "\n"
            print numeric_data
            print "\n\n\n"
            item_extension_location = files_by_class[
                class_options[option]][index].rfind(".")
            item_no_extension = files_by_class[class_options[option]][
                index][:item_extension_location]
            plt.xlim(0, len(numeric_data))
            # plt.subplot()
            ax_array[index].bar(list(xrange(len(numeric_data))), numeric_data[
                :], color=colors[option])
            # plt.tight_layout()
        plt.savefig(path_to_save_to + class_options[option] + ".pdf")

    return 0

if __name__ == "__main__":
    test_path = "/home/rkaehler/NSV Test/"
    training_path = "/home/rkaehler/NSV Train/"
    output_path = "/home/rkaehler/Histograms Test/"
    jGrassClasses = ["Transition", "Native", "Invaded"]
    # make_historgram(training_path, jGrassClasses, output_path)
    make_bar_char_from_data_matrix_and_isolate_dimensions(
        training_path, jGrassClasses, output_path)
