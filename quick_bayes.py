"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""

import multiprocessing
import shutil

import processFastaAll
import jGrassAvgAndRep
import jGrassFileMerger
import dataSlicer
# import bayes_driver
import naive_bayes
import commonFunctions
import argparse
import bayes_crossfold

def run_folds(machine_name=None, test_size=None):
    """ run the cross fold verification for the naive bayes metagenomic tests"""
    if machine_name == None:
        machine_name = "rkaehler"

    # must have starting data set
    main_path = "/home/" + machine_name

    johnson_grass_data_path = main_path + "/Johnson Grass Fasta/"

    raw_train = main_path + "/JGrass Train/"

    raw_test = main_path + "/JGrass Test/"

    j_grass_classes = ["Transition", "Native", "Invaded"]

    training_order, test_order = commonFunctions.mix_files(
        johnson_grass_data_path, j_grass_classes)

    if test_size == None:
        test_size = len(training_order)
    final_matrix = {}

    # final_output = ""
    # knowledge of classes (list?)
    # list of file paths
    mer_size = 4
    step_size = 1
    # worker pool (necessary?)

    frag_limit = 400000
    parts = 10


    n_bins = 3
    m_est = 0.5
    # workers should be 3x number of parts, 3 classes each with n parts
    workers = 3 * parts
    my_pool = multiprocessing.Pool(workers)
    n_dims = 20

    for index in xrange(2):
        commonFunctions.clearOutputPath(raw_train)
        for elt in training_order[index]:
            ini_location = johnson_grass_data_path + elt
            final_location = raw_train + elt
            shutil.copy2(ini_location, final_location)

        commonFunctions.clearOutputPath(raw_test)
        for alt in test_order[index]:
            ini_location = johnson_grass_data_path + alt
            final_location = raw_test + alt
            shutil.copy2(ini_location, final_location)

        # bayes_crossfold.prep_data(mer_size, step_size,
        #           frag_limit, parts, my_pool, machine_name)
        results = bayes_driver(machine_name, n_dims, n_bins, m_est)

        # res_keys = results.keys()
        # # print res_keys
        # for key in res_keys:
        #     # print "the key is\t",key
        #     # print results[key]
        #     # print type(results[key])
        #     item_keys = results[key].keys()
        #     # print item_keys
        #     for item in item_keys:
        #         # print item
        #         # print item,"\t",results[key][item]
        #         try:
        #             final_matrix[key][item] += results[key][item]
        #         except:
        #             try:
        #                 # final_matrix[key][item].update()
        #                 final_matrix[key][item] = {}
        #                 final_matrix[key][item] = results[key][item]

        #                 # final_matrix[key] = {}
        #             except:
        #                 final_matrix[key] = {}
        #                 final_matrix[key][item] = {}
        #                 final_matrix[key][item] = results[key][item]
        # print "index number is\t", index
        # print "\ncurrnet final matrix looks like\n", final_matrix, "\n"

    # print "\n\n\n"
    # print "ending matrix"
    # print final_matrix

def bayes_driver(machine_name, n_dims, n_bins, m_est):
    "" " call this function to run the driver " ""
    # machine_name = "rkaehler"

    #must have starting data set
    main_path = "/home/" + machine_name

    final_train = main_path + "/Final Train/"
    final_test = main_path + "/Final Test/"


    #input for metagenomic processing
    final_train_output = final_train + "trainingNormalized.csv"

    final_test_output = final_test + "testingNormalized.csv"

    class_list = ["0", "1", "2"]
    #j_grass_classes = ["Transition", "Native", "Invaded"]

    # class_list = ['1','2','3','4']
    # n_bins = 3
    # m_est = 0.10
    c_res = external_request(final_test_output, final_train_output,\
    n_bins, class_list, n_dims, m_est)
    return c_res

def external_request( main_path, votingSize, n_dims):
    """ call this function to run the driver """

    final_train = main_path + "/Final Train/"
    final_test = main_path + "/Final Test/"

    #input for metagenomic processing
    final_train_output = final_train + "trainingNormalized.csv"
    final_test_output = final_test + "testingNormalized.csv"

    training_data = commonFunctions.data_parser(final_train_output)
    test_data = commonFunctions.data_parser(final_test_output)

    training_data = commonFunctions.make_data_numeric(training_data[:])
    test_data = commonFunctions.make_data_numeric(test_data[:])

    # theTen = select_dims(training_data, n_dims)
    
    if n_dims < (len(test_data[0]) - 1):
        use_dims = select_dims(training_data[:], n_dims)

        training_data = commonFunctions.pull_subset_of_data_keep_class_vect(use_dims[:], training_data[:])

        test_data = commonFunctions.pull_subset_of_data_keep_class_vect( use_dims[:], test_data[:])
    else:
        print "using all dims"
    # print "\n\n\n"


def main():
    parser = argparse.ArgumentParser(prog='Quick Bayes for debugging')
    parser.add_argument("--machine_name", nargs="?", default=None)
    parser.add_argument("--num_runs", nargs="?", default=None)

    args = parser.parse_args()

    if args.machine_name or args.num_runs:
        run_folds(args.machine_name, int(args.num_runs))
    else:
        run_folds()


if __name__ == "__main__":
    main()