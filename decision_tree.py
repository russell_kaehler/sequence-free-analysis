import math
"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Comuter Science Grad Student
Thesis Research 
Decision Tree 

This class builds a decision tree from a set of data. 

"""

"""
------ Make a map of attTypes to counts
"""
def map_attType_counts( dataSet, chosenLabel):
    
    #make copy of data set
    myData= dataSet[:]
    myValueMap = {}

    for row in myData:
        #increment count of att type if seen before
        if ( myValueMap.has_key(row[chosenLabel])):
            myValueMap[row[chosenLabel]] += 1.0

        #else set att type count to one 
        else:
            myValueMap[row[chosenLabel]] = 1.0

    #return map of counts
    return myValueMap




"""
------ Get the label of the att, from the labels list, with the greatest information gain
"""
def get_best_att( dataSet, attLabels, diagnosisLabel):
    
    myData = dataSet[:]
    
    #stat best val at zero 
    bestVal = 0.0
    
    #assume there is no best label
    bestAtt = None


    for label in attLabels:
        #is label is not class label - data set dependant!
        if label != attLabels[-1]:
            #calculate the information gain in the current dimension
            dimGain = calculate_gain(myData, label, diagnosisLabel)
            #if dimensional information gain is greater than or equal to current best value 
            if dimGain >= bestVal:
                #set labels and value to current value
                bestVal = dimGain
                bestAtt = label

    #return the label of the best att
    return bestAtt




"""
------ Get entropy of given dataset and diagnosis class label
"""
def get_entropy( dataSet, diagnosisLabel):
    
    myData = dataSet[:]

    #generate att count map of current data
    attValueMap = map_attType_counts( myData, diagnosisLabel)

    #initialize entropy val at zero
    myEntropy = 0.0

    for counts in attValueMap.values():
        #calculate percent of this count out of the total number of counts
        myProb = counts/ sum( attValueMap.values())
        
        #calcualte entropy of of current count
        myEntropy += (-1) * myProb * math.log( myProb, 2)

        #think of count as a more genric represenation of p+ or p- where p can have more than + or - 
        #thus can find entropy of data set that has n outcomes
    return myEntropy

     


"""
------ Find information gain in the given dimension 
"""
def calculate_gain( dataSet, attLabel, diagnosisLabel):
    
    myData = dataSet[:]

    #generate map of current atts
    attValueMap = map_attType_counts( myData, attLabel)

    attSubsetEntropies = 0.0 

    #gets total entropy of current data set
    totalEntropy = get_entropy(myData, diagnosisLabel)


    for key in attValueMap.keys():
        #get vi/ |v|
        attValueProb = attValueMap[key] / sum( attValueMap.values())

        #find list of records in current data that have same value
        myAttSubset = get_same_value( myData, attLabel, key) 
        
        #sum att type entropies 
        attSubsetEntropies += attValueProb *  get_entropy(myAttSubset, diagnosisLabel)

    #total info gain is totalEntropy of att - Sum of all vi entropies
    myInfoGain = totalEntropy - attSubsetEntropies
    #return infogain in dimension
    return myInfoGain
    


"""
------ Get Elts of Same value 
"""


def get_same_value( dataSet, label, value):
    

    myData = dataSet[:]

    #list of elts that have the same value in some dimension
    mySameElts = []

    #iterate over data set
    for elt in myData:
        #if myelt has value in dimension equal to the dimension and value we seek
        if elt[label] == value:
            #add such a desierable element to out list of same value elts
            mySameElts.append(elt)
    #return the wonderful list 
    return mySameElts


"""
------ tree node - Tree Data Strucutre
"""
class TreeNode:
    #each node has a label and a set of children 
    #could be improved by recording parent node - not required for project, but useful in future implementations of TDS
    def __init__(self, labelIN, children= None):
        self.label = labelIN
        self.children = children or {}

    #node can print self
    def __repr__(self):
        return self.__str__()
        
    #via calling 
    def __str__(self):
        return "label is "+ self.label



"""
------ build decision tree based off of training data
"""


def tree_builder( dataSet, labelsList, diagnosisLabel):

    #make copy of current data, better practices
    myData = dataSet[:]

    #get map of current att type counts
    currentOutcomeMap = map_attType_counts( myData, diagnosisLabel)

    #most likely outcome count 
    outcomeValue = 0.0
    #label of most likely outcome
    mostLikelyOutcome = None
    
    
    for key in currentOutcomeMap.keys():
        #see if current key has greater count than current outcome
        if currentOutcomeMap[key] >= outcomeValue:
            #if so set labels accordingly
            outcomeValue = currentOutcomeMap[key]
            mostLikelyOutcome = key

    #print "This is what the outcome map looks like"
    #print currentOutcomeMap

    #if list of labels has run out
    if not dataSet or (len(labelsList)-1) <= 0:
        #print "no atts left in att set" 
        noAttsLeft = TreeNode( mostLikelyOutcome, {})
        return noAttsLeft

    #if data has homogeneous label (outcome)
    elif len(currentOutcomeMap.keys()) == 1:
        #print "unanimous data set encountered"
        unanimousData = TreeNode( mostLikelyOutcome, {})
        return unanimousData
    
    else:   
        #calculate next att to look through
        myNextBestAtt = get_best_att(myData, labelsList, diagnosisLabel)
    
        #get map of ( next label to outcomes )
        attValueMap = map_attType_counts(myData, myNextBestAtt)
        
        #print" my next att will be ", myNextBestAtt
        #print " this is my att value map"
        #print attValueMap      


        #create new tree node
        myTree = TreeNode( myNextBestAtt, mostLikelyOutcome)

        

        #print myTree.label, " my label is currently ----- "
        #create new list of labels - remove previous best att from list of labels to 'look' through
        newLabelList = []
        
        #do the work of removing said label
        for label in labelsList:
            #if label isn't old best att
            if label != myNextBestAtt:
                #print label, "\t", myNextBestAtt

                #do the bold thing and add the label to the list of unused labels
                newLabelList.append(label)


        #subtrees, children, map
        subTrees = {}
        
        
        for key in attValueMap.keys():
            #recursive call to build a subtree on the current next label 
            mySubTree = tree_builder( get_same_value( myData, myNextBestAtt, key), newLabelList, diagnosisLabel)


            #print mySubTree, " this is the subtree" 
            #print key, " -------- - -------- ---- ------ - is key"


            #add subtree to list of subtrees
            subTrees[key] = mySubTree

        #set my curent tree node's children as the map of subtrees
        myTree.children = subTrees
        
        return myTree
    




"""
------ classify training data according to pre-built tree
"""

def classify_data(dataSet, labels, aTree):
    
    myData = dataSet[:]

    #build map of results
    classificationResults = {}

    # key 0 is for incorrect classification counts
    classificationResults[0] = 0
    #key 1 is for correct results
    classificationResults[1] = 0
    #key 2 counts number of items unable to be classified
    classificationResults[2] = 0

    #loop over data records
    for elt in myData:

        #get hypothetical outcome from decision tree
        myResult = get_result(elt, aTree)

        #get value of current elts diagnosis
        eltDiagnosis = elt[labels[-1]]

        #adjust count of classification results accordingly 
        if myResult == '2':
            classificationResults[2] += 1       
        elif myResult == eltDiagnosis:
            classificationResults[1] += 1
        elif myResult != eltDiagnosis:
            classificationResults[0] += 1

    #return results, feels good
    return classificationResults





"""
------ generate outcome for test record
"""
def get_result(elt, aTree):
    
    #supoose there is only one key
    if len(aTree.children.keys()) <= 1:
        #return the label of the tree, the outcome
        return aTree.label


    else:
        #proceed down tree using the current label as your guide
        myNextAtt = aTree.label

        #print myNextAtt, " with val ", elt[myNextAtt]
        
        #if the value of your current label isn't in the list of children 
        #ie the tree has no mapping to tell you the result
        #unclassifiable
        if elt[myNextAtt] not in aTree.children.keys():
            #return 2 so that you know you didn't classify it
            return '2'

        #print aTree.children
    
        #print "\n\n"

        #if you have a move to make
        nextTree = aTree.children[elt[myNextAtt]]
        #proceed to the next tree, recursive
        return get_result(elt, nextTree)




"""
------ recursivly print a pre-built tree
"""
def print_my_tree( myTree, myString):
    
    #print type(myTree), "tree is"
    #print type(myTree.children), " children are"
    
    #print formating string and current node label
    print "%s look in %s" % ( myString, myTree.label)

    #print children treees
    for nextAtt in myTree.children.keys():
        #print current formatting string and the label of the next place to look
        print "%s\t if value is %s" % ( myString, nextAtt)
        #call print function of child node 
        print_my_tree( myTree.children[nextAtt], myString+"\t")




"""
------ Get att labels from file-pulls labels off of current file use them as keys in dict look uplater
"""
def gather_atts( pathToFile):
    
    #read in file
    with open( pathToFile, 'r') as myFile:
        #get first line remove formatting 
        labels = myFile.readline().replace('\r\n','')

    #generate list of label names given the label line
    labelList = []
    #split list on ,'s givving full string names
    for name in labels.split(','):
        #add name to labels list
        labelList.append(name)
    #return names of atts
    return labelList


"""
------ get data from file, needs labels already know!
"""
def data_parser( pathToFile, myLabels):
    
    #build list of data records
    myDataSet = []
    #open file read each line removing \r\n and place that into a list of lines
    with open( pathToFile, 'r') as myFile:
        #loop over lines in file
        for line in myFile.readline():
            #read line ~lr to above
            currentLine = myFile.readline().replace('\r\n','')
            tempList = []
            #if line has more than one split
            if len(currentLine.split(',')) > 1:
                #loop over what the split fn returns
                for value in currentLine.split(','):
                    #add them values to your temp list
                    tempList.append(value)
                #do the python thing and zip labels in a 1-1 mapping to your temp list
                #use that as a mapping for a dict and add that dict to your list!
                # B A M ! 
                myDataSet.append(dict(zip(myLabels, tempList)))
    #remove the line with labels

    
    #build data dictionary of lines with label value pairs
            
    #delete record where label maps to label - boring! - - also confuses classifier, if not done = (
    del myDataSet[0]
    return myDataSet



"""
------ Main
"""
if __name__ == "__main__":
    #pull labesl from training data - in folder! 
    myLabels = gather_atts( "/Users/russkaehler/Machine Learning Spring 2012/KaehlerDecisionTree/train.txt")
    
    #diagnosis label is last label in list
    diagnosisLabel = myLabels[-1];

    #build training data for python, ie read file
    trainingData = data_parser( "/Users/russkaehler/Machine Learning Spring 2012/KaehlerDecisionTree/train.txt", myLabels)

    #build decision tree from training data, labels, and the diagnosis label
    myDecisionTree = tree_builder(trainingData, myLabels, diagnosisLabel)

    #print the training tree! 
    print " Training Tree Looks Like "

    #do the printing here
    print_my_tree(myDecisionTree, " ")

    #read in test data
    testData = data_parser( "/Users/russkaehler/Machine Learning Spring 2012/KaehlerDecisionTree/test.txt", myLabels)

    #test the test data - have returned as a map of 0 -> wrong, 1-> correct, 2 -> unable to classify all are counts
    classificationResults = classify_data(testData, myLabels, myDecisionTree)
    
    #my total correct is the number that the key one maps to in the resulsts map
    totalCorrect = classificationResults[1]
 
    #~lr with 0 -> total number incorrect
    totalWrong = classificationResults[0]

    #total unable to classify <- 2
    totalUnableToClassify = classificationResults[2]

    #total number attempted to classify is the total of wrong + correct + unable to understand
    mySum = totalCorrect+ totalWrong+ totalUnableToClassify
    percentScore = totalCorrect/((totalCorrect+totalWrong)*1.0)
    
    #print them results!
    print "number correct",totalCorrect,", total wrong",totalWrong,", couldn't classify",totalUnableToClassify," out of a total",mySum
    print "percentage is", percentScore






