"""
Copyright (c)  Russell Kaehler 2016
University of Montana
Computer Science Grad Student
"""

import sys
import multiprocessing
from os import listdir

import commonFunctions
import index_to_mer
"""
# This takes fasta files and changes them to 4**Mer size NSV files saved
# as .txt
"""


def mer_index_finder(my_string, mer_size):
    # my_string = my_string.lower()
    char_value = {}
    char_value["a"] = 0
    char_value["t"] = 1
    char_value["c"] = 2
    char_value["g"] = 3
    i = 0
    j = 0
    base_four_string = ""

    # myStrLen = len(my_string)
    while(i < mer_size):
        base_four_string += str(char_value[my_string[i]])
        i += 1

    index = int(base_four_string, 4)

    return index


def mer_to_base_four(my_string, mer_size=None):
    # my_string = my_string.lower()
    char_value = {}
    char_value["a"] = 0
    char_value["t"] = 1
    char_value["c"] = 2
    char_value["g"] = 3
    i = 0
    j = 0
    base_four_string = ""

    if not mer_size:
        mer_size = len(my_string)

    while(i < mer_size):
        base_four_string += str(char_value[my_string[i]])
        i += 1

    return int(base_four_string)


def make_collapse_map(mer_size):

    num_mers = 4 ** mer_size
    complment_set = set()
    for i in xrange(num_mers):
        my_mer = index_to_mer.external_request(i, mer_size)

        complment_set.add(my_mer)
        rev_c_mer = make_complement_mer(my_mer)

        if (rev_c_mer in complment_set) and (rev_c_mer != my_mer):
            # print my_mer,"\t", rev_c_mer
            complment_set.remove(my_mer)

    ordered_set = sorted(list(complment_set), key=mer_to_base_four)

    ordered_map = dict(zip(ordered_set, xrange(len(ordered_set))))

    return ordered_map


def get_mer_count_use_complement(mer_size, file_fragments, slidingSize):
    mer_counts = {}
    for fragment in file_fragments:
        j = 0
        max_j = len(fragment) - mer_size
        while(j < max_j):
            mer_frag = fragment[j:j + mer_size]
            mer_frag = mer_frag.lower()
            if("n" not in mer_frag):
                try:
                    mer_counts[mer_frag] += 1
                except:
                    mer_counts[mer_frag] = 1
                # myNSV[mer_index] += 1
                # myNSV[compliment_index] += 1
            j += slidingSize

    array_size = (4 ** mer_size) / 2
    if mer_size % 2 == 0:
        array_size += 2**(mer_size - 1)

    mer_to_index = make_collapse_map(mer_size)

    myNSV = [0] * array_size
    for mer in mer_counts.keys():
        try:
            mer_index = mer_to_index[mer]
        except KeyError:
            mer_index = mer_to_index[make_complement_mer(mer)]

        myNSV[mer_index] += mer_counts[mer]
        # myNSV[mer_index] = mer_counts[mer]

    return myNSV[:]


def get_mer_count(mer_size, file_fragments, slidingSize):
    mer_counts = {}
    for fragment in file_fragments:
        j = 0
        max_j = len(fragment) - mer_size
        while(j < max_j):
            mer_frag = fragment[j:j + mer_size]
            mer_frag = mer_frag.lower()
            if("n" not in mer_frag):
                try:
                    mer_counts[mer_frag] += 1
                except:
                    mer_counts[mer_frag] = 1
                # myNSV[mer_index] += 1
                # myNSV[compliment_index] += 1
            j += slidingSize

    array_size = 4**mer_size

    myNSV = [0] * array_size
    for mer in mer_counts.keys():
        mer_index = mer_index_finder(mer, mer_size)
        myNSV[mer_index] += mer_counts[mer]

    return myNSV[:]


def get_fragment_count(reference_list):
    mer_size = reference_list[0]
    fragment = reference_list[1]
    slidingSize = reference_list[2]
    mer_counts = {}

    j = 0
    max_j = len(fragment) - mer_size
    while(j < max_j):
        mer_frag = fragment[j:j + mer_size]
        mer_frag = mer_frag.lower()
        if("n" not in mer_frag):
            try:
                mer_counts[mer_frag] += 1
            except:
                mer_counts[mer_frag] = 1
            # myNSV[mer_index] += 1
            # myNSV[compliment_index] += 1
        j += slidingSize

    mer_to_index = make_collapse_map(mer_size)

    myNSV = [0] * array_size
    for mer in mer_counts.keys():
        try:
            mer_index = mer_to_index[mer]
        except KeyError:
            mer_index = mer_to_index[make_complement_mer(mer)]

        myNSV[mer_index] += mer_counts[mer]

    return myNSV[:]


def make_complement_mer(mer_string):
    nu_mer = ""
    compliment_map = {"a": "t", "c": "g", "t": "a", "g": "c"}
    for base in mer_string:
        nu_mer += compliment_map[base]
    nu_mer = nu_mer[::-1]
    return nu_mer[:]


def read_file(file_path):
    frags = []
    with open(file_path, "r") as myFile:
        for line in myFile:
            first_char = line[0]
            if(first_char != ">"):
                frags.append(line)
    return frags[:]


def processFiles(incomingItems):
    inputFile = incomingItems[0]
    # print inputFile, "\t this is the lhs"
    fileItem = incomingItems[1]
    # print fileItem, "\t This is my individual file item"
    outputFile = incomingItems[2]
    mer_size = incomingItems[3]
    slidingSize = incomingItems[4]
    tag = ""
    tagCounts = 0
    frag = ""
    fragCounts = 0
    tagFragDict = {}

    file_string = inputFile + fileItem

    fragments = read_file(file_string)

    nsvDict = {}

    nsvOut = outputFile + str(mer_size) + "Mer_" + fileItem[:-6] + ".txt"

    # myNSV = get_mer_count(mer_size, fragments, slidingSize)
    myNSV = get_mer_count_use_complement(mer_size, fragments, slidingSize)

    # nsvDict[key] = myNSV
    nsvStr = ""
    for x in range(0, len(myNSV)):
        if( nsvStr == ""):
            nsvStr += str(myNSV[x])
        else:
            nsvStr += "," + str(myNSV[x])

    with open(nsvOut, "w") as myOut:
        myOut.write(nsvStr + "\n")


def process_file_map_on_fragments(incomingItems):
    inputFile = incomingItems[0]
    # print inputFile, "\t this is the lhs"
    fileItem = incomingItems[1]
    # print fileItem, "\t This is my individual file item"
    outputFile = incomingItems[2]
    mer_size = incomingItems[3]
    slidingSize = incomingItems[4]
    pool_reference = incomingItems[5]
    tag = ""
    tagCounts = 0
    frag = ""
    fragCounts = 0
    tagFragDict = {}

    file_string = inputFile + fileItem

    fragments = read_file(file_string)

    nsvDict = {}

    nsvOut = outputFile + str(mer_size) + "Mer_" + fileItem[:-6] + ".txt"

    # myNSV = get_mer_count(mer_size, fragments, slidingSize)


    array_size = (4 ** mer_size) / 2
    if mer_size % 2 == 0:
        array_size += 2**(mer_size - 1)

    request_list = []
    for fragment in fragments:
        request_list.append([mer_size, fragment, slidingSize, array_size])
    
    pool_reference.map(get_mer_count_use_complement, request_list[:])
    # nsvDict[key] = myNSV
    nsvStr = ""
    for x in range(0, len(myNSV)):
        if( nsvStr == ""):
            nsvStr += str(myNSV[x])
        else:
            nsvStr += "," + str(myNSV[x])

    with open(nsvOut, "w") as myOut:
        myOut.write(nsvStr + "\n")

def externalRequest( inputFile, outputFile, myPool, mer_size, slidingSize):
    # print "Hit external request of process fasta all"
    filesOne = commonFunctions.getFiles( inputFile)

    checkOutput = commonFunctions.clearOutputPath(outputFile)

    listOne = []
    # package data for multiprocessing
    for i in range(0, len(filesOne)):
        miniList = [inputFile, str(filesOne[i]), outputFile, mer_size, slidingSize]
        listOne.append(miniList[:])
    myPool.map(processFiles, listOne)
    
    return 0


def external_request_map_on_fragment( inputFile, outputFile, myPool, mer_size, slidingSize):
    # print "Hit external request of process fasta all"
    filesOne = commonFunctions.getFiles( inputFile)

    checkOutput = commonFunctions.clearOutputPath(outputFile)

    listOne = []
    # package data for multiprocessing
    for i in range(0, len(filesOne)):
        miniList = [inputFile, str(filesOne[i]), outputFile, mer_size, slidingSize, myPool]
        process_file_map_on_fragments(miniList[:])
    
    return 0


if __name__ == "__main__":
    inputFileOne = "/home/socrates/Spring 2015 Bioinformatics Research/Human/Fasta Files/"

    outputFileOne = "/home/socrates/Spring 2015 Bioinformatics Research/Human/Human Gut NSV/8Mer/"

    filesOne = getFiles( inputFileOne)

    poolSize = 12
    mer_size = 8

    myPool = multiprocessing.Pool(poolSize)
    listOne = []
    for i in range(0, len(filesOne)):
        miniList = []
        print "my file is \t", filesOne[i]
        miniList.append(inputFileOne)
        miniList.append(str(filesOne[i]))
        miniList.append(outputFileOne)
        miniList.append(mer_size)
        listOne.append(miniList)
    

    myPool.map(processFiles, listOne)






