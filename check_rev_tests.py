"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""
import time
import index_to_mer
import processFastaAll


def swap_base(index_int, base):
    if index_int < 0:
        return '-' + swap_base(-index_int, base)
    else:
        (d, m) = divmod(index_int, base)
        if d:
            return swap_base(d, base) + digit_to_char(m)
        else:
            return digit_to_char(m)
    # return mer


def make_base_four_both_ways():
    mer_size = 3
    nu_time = 0.0
    old_time = 0.0
    while mer_size < 4:
        num_mers = 4 ** mer_size
        # complment_set = set()
        for i in xrange(num_mers):
            # my_mer = index_to_mer.external_request(i, mer_size)
            mer_as_num = int2base(i, 4)
            print mer_as_num


def int2base(x, base):
    digs='0123456789abcdefghijklmnopqrstuvwxyz'
    if x < 0:
        sign = -1
    elif x == 0:
        return digs[0]
    else:
        sign = 1
    x *= sign
    digits = []
    while x:
        digits.append(digs[x % base])
        x /= base
    if sign < 0:
        digits.append('-')
    digits.reverse()
    return ''.join(digits)


def compute_collapse():
    mer_size = 2
    while mer_size < 4:
        start_nu = time.time()
        num_mers = 4 ** mer_size
        complment_set = set()
        for i in xrange(num_mers):
            my_mer = index_to_mer.external_request(i, mer_size)

            complment_set.add(my_mer)
            rev_c_mer = processFastaAll.make_complment_mer(my_mer)

            if (rev_c_mer in complment_set) and (rev_c_mer != my_mer):
                # print my_mer,"\t", rev_c_mer
                complment_set.remove(my_mer)

        ordered_set = sorted(list(complment_set), key=mer_index_finder)

        ordered_map = dict(zip(ordered_set, xrange(len(ordered_set))))

        mer_size += 1


def mer_to_base_four(my_string, mer_size=None):
    # my_string = my_string.lower()
    char_value = {}
    char_value["a"] = 0
    char_value["t"] = 1
    char_value["c"] = 2
    char_value["g"] = 3
    i = 0
    j = 0
    base_four_string = ""

    if not mer_size:
        mer_size = len(my_string)

    while(i < mer_size):
        base_four_string += str(char_value[my_string[i]])
        i += 1

    return int(base_four_string)

def time_complments():
    mer_size = 3
    nu_time = 0.0
    old_time = 0.0
    while mer_size < 5:
        num_mers = 4 ** mer_size
        # complment_set = set()
        for i in xrange(num_mers):
            my_mer = index_to_mer.external_request(i, mer_size)
            # print my_mer
            # mer_index = swap_base(i, mer_size)

            # complment_set.add(my_mer)

            start_nu = time.time()
            rev_c_mer_a = processFastaAll.make_complment_mer_new(my_mer)
            end_nu = time.time()
            nu_time += end_nu - start_nu

            stat_old = time.time()
            rev_c_mer_b = processFastaAll.make_complment_mer(my_mer)
            end_old = time.time()
            old_time += end_old - stat_old

            if rev_c_mer_a != rev_c_mer_b:
                print rev_c_mer_a, "\t", rev_c_mer_b
                print "it's broken..."
            # if (rev_c_mer in complment_set) and (rev_c_mer != my_mer):
            #     # print my_mer,"\t", rev_c_mer
            #     complment_set.remove(my_mer)

        mer_size += 1
    print "new method took %s sec" % (nu_time)
    print "old method took %s sec" % (old_time)


if __name__ == "__main__":
    compute_collapse()
