#Russell Kaehler
#Fall 2013 Bioinformatics
#Graduate Research Project

import math
import scipy
from scipy import stats
"""
------ get data from file, needs labels already know!
"""
def data_parser( pathToFile):
	
	#build list of data records
	myDataSet = []
	myClassSet = []
	#open file read each line removing \r\n and place that into a list of lines
	with open( pathToFile, 'r') as myFile:
		#loop over lines in file
		for line in myFile:
			#read line ~lr to above
			currentLine = line.replace('\r\n','').strip()
			tempList = []
			#if line has more than one split
			if len(currentLine.split(',')) > 1:
				#loop over what the split fn returns
				for value in currentLine.split(','):
					#add them values to your temp list
					tempList.append(value)
				#do the python thing and zip labels in a 1-1 mapping to your temp list
				#use that as a mapping for a dict and add that dict to your list!
				# B A M ! 
				myClassSet.append(tempList[-1])
				del(tempList[-1])
				myDataSet.append(tempList)
	#remove the line with labels

	
	#build data dictionary of lines with label value pairs
			
	#delete record where label maps to label - boring! - - also confuses classifier, if not done = (
	#del myDataSet[0]
	return myDataSet, myClassSet
	
def find_Manhattan( pointA, pointB):
	dist = 0
	#print pointA
	for x in range(0,len(pointA)):
		dist += math.fabs(float(pointA[x]) - float(pointB[x]))
	return dist

def find_T_score( dataSet, classInfo):
	tScores = []
	pValues = []
	newData = []
	if len(dataSet) > 0:
		#loop over cols make a transposed data set
		classSet = set(classInfo)
		#classDict = {}
		mapClass = {}
		for i in classSet:
			tmpIndicies = [j for j, x in enumerate(classInfo) if x == i]
			print tmpIndicies , "\t temp indicies"
			#classDict[i] = tmpIndicies
			for x in tmpIndicies:
				mapClass[x] = i
		for col in range(0, len(dataSet[0])):
			subSetHolder = {}
			#for levels of classes
			#take indicies and transform them into sub data sets
			for i in range(0,len(dataSet)):
				classI = mapClass[i]
				if classI in subSetHolder.keys():
					tmpList = subSetHolder[classI]
					tmpList.append(float(dataSet[i][col]))
					subSetHolder[classI] = tmpList
				else:
					nuList = [float(dataSet[i][col])]
					subSetHolder[classI] = nuList
			#if col < 10:
				#print subSetHolder
			myKeys = sorted(subSetHolder.keys())
			count = 1.0
			tmpT = 0.0
			tmpP = 0.0
			for x in range(0,len(myKeys)-1):
				xData = subSetHolder[myKeys[x]]
				for y in range(x+1, len(myKeys)):
					yData = subSetHolder[myKeys[y]]
					#print x,"\t",y
					#print xData
					#print yData
					#print "\n\n"
					#print xData
					#print yData
					tRes, pVal = stats.ttest_ind(xData, yData)
					tRes = math.fabs(tRes)
					pVal = math.fabs(pVal)
					tmpT += tRes
					tmpP += pVal
				count += 1.0
			myColT = tmpT / count
			myColP = tmpP / count
			
			tScores.append(myColT)
			pValues.append(myColP)
			#old method ~~~~~~~~~~~~~~~~~~~~~~~~ old ~~~~~~~~~~~old
			#for row in range(0,len(dataSet)):
			#	newRow.append( dataSet[row][col]):
			#~~~~~~~~~~~~~~~~~~~~~~~~ old ~~~~~~~~~~~old	
			#newData.append(newRow)
		#~~~~~~~~~~~~~~~~~~~~~~~~ old ~~~~~~~~~~~old	
		#for rowOne in range(0, len(newData)):
			#for rowTwo in range(0, len(newData)):
			#if rowOne != rowTwo:
				#tRes, pVal = scipy.stats.ttest_ind(newData[rowOne], newData[rowTwo])
		#~~~~~~~~~~~~~~~~~~~~~~~~ old ~~~~~~~~~~~old ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^	
		#print classDict	
		#print mapClass
	print "there are no errors"
	return tScores, pValues
	



#data file
startingFile = "normalJohnsonGrassDataWithClassVect.csv"

rawData, dataClasses = data_parser( startingFile)
print rawData[0][0:2]
print rawData[0][-1]
print len(rawData), "\t there's so many lines in the raw data"
print len(dataClasses), "\t There's this many lines in the data class object"
print len(rawData[0])
print len(rawData[1])

print "Making a dist test"
distTest = find_Manhattan(rawData[0], rawData[1])
print distTest


myTScores, myPValues = find_T_score( rawData, dataClasses)
myTen = {}
print "This is the len of the T scores \t", len(myTScores)
for i in range(0,len(myTScores)):
	val = myTScores[i]
	if len(myTen) < 10:
		myTen[val] = i
	else:
		minValue = min(myTen)
		minLocation = myTen[minValue]
		#minLocation = myTScores[pseudoKey]
		#print val, "\t", minValue
		if val > minValue:
			del(myTen[minValue])
			myTen[val] = i
			
print myTen	


dataSubset = []
keyList = myTen.keys()
for x in range(0,len(keyList)):
	col = myTen[keyList[x]]
	for row in range(0,len(rawData)):
		if x == 0:
			dataSubset.append([])
		val = rawData[row][col]
		dataSubset[row].append(val)
		
		
#print dataSubset 
outFileName = "12MerSubestBestFeatures.csv"
with open(outFileName, "w") as myOut:
	for row in range(0,len(dataSubset)):
		for col in range(0,len(dataSubset[0])):
				myOut.write(dataSubset[row][col] + ",")
		myOut.write(dataClasses[row] + "\n")
	
		
		
		
distAB = []
for elt in range(0,len(dataSubset)):
	tempList = []
	for alt in range(0, len(dataSubset)):
		if alt == elt:
			tempList.append(0)
		else:
			dist = find_Manhattan(dataSubset[elt], dataSubset[alt])
			tempList.append(dist)
	distAB.append(tempList)

	

winners = []
votingSize = 1
for i in range(0,len(distAB)):
	myList = distAB[i]
	#print i
	tempDict = {}
	for j in range(0,len(distAB[i])):
		if j != i:
			ijDist = distAB[i][j]
			
			if len(tempDict.keys()) < votingSize:
				tempDict[j] = ijDist
			else:
				maxValue = max(tempDict.values())
				#print maxValue
				valueRow = -1
				for key in tempDict.keys():
					if maxValue == tempDict[key]:
						valueRow = key
						
				if (ijDist < maxValue) and (valueRow != -1):
					del(tempDict[valueRow])
					tempDict[j] = ijDist
	winners.append(tempDict) 

results = [[0,0,0],[0,0,0],[0,0,0]]
print results
	
for i in range(1, len(winners)):
	rowDict = winners[i]

	rowKeys = rowDict.keys()
	itemClass = dataClasses[i]
	votes = {}
	for x in range( 0, len(rowKeys)):
		rowClass = dataClasses[rowKeys[x]]
		if rowClass not in votes.keys():
			votes[rowClass] = 1
		else:
			votes[rowClass] += 1
			
	maxVotes = -1
	predClass = -1
	for x in votes.keys():
		if maxVotes < votes[x]:
			maxVotes = votes[x]
			predClass = x
	results[int(itemClass)][int(predClass)] += 1
	print i,"\t",itemClass,"\t",predClass
		
print results
		
		
		
