"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""

import sys
import multiprocessing
import itertools
from os import listdir

import commonFunctions

def makeARep(lineList):
	myRep = []
	# print "line list\t", type(lineList)
	denom = len(lineList) 
	for i in xrange(0, len(lineList[0])):
		tempList = []
		for n in xrange(0,denom):
			temp = lineList[n][i]
			tempList.append(temp)
		
		avg = sum(tempList) / (denom * 1.0)
		myRep.append(avg)
	return myRep


#normalize by max min
def normalizeVals(lineStarting, lineMin, lineMax):
	resList = []
	for i in xrange(0, len(lineStarting)):
		iniVal = int(lineStarting[i])
		fixedVal = (iniVal - lineMin) / (lineMax - lineMin)
		resList.append(fixedVal)
	# print len(resList)
	return resList
	

#normalize by number of mers observed
#~~~~~~~~This function can be paralelized once you have the mer numbers ~~~~~~~ !!!!!! ~~~~~
#an attempt to multi thread this has been done, and SLOWS the program down
#do not multi thread again
def normalizeBySize(splitLine):
	resList = []
	numMers = sum(splitLine)
	for i in xrange(0, len(splitLine)):
		cVal = splitLine[i] / (1.0 * numMers)

		resList.append(cVal)
	return resList


"""
~~~~~~~~~~~~~~ Don't use
"""
def normalizeWorker(inList):
	num = inList[0]
	denom = inList[1] * 1.0
	return num/denom	
	

def writeLineToFile( dest, lineList):
	with open(dest, 'w') as myFile:
		for n in xrange(0, len(lineList)):
			myCount = 0
			line = lineList[n]
			lineLen = len(line)
			for i in xrange(0,lineLen):
				val = str(line[i])
				if (i < (lineLen-1)):
					myFile.write( val + ",")
					myCount += 1
				else:
					myFile.write( val + "\n")
					myCount += 1
			# print myCount
			

def writeRepToFile(dest, myRep):
	with open(dest, 'w') as myFile:
		myCount = 0
		repLen = len(myRep)
		for i in xrange(0, repLen):
			val = str(myRep[i])
			if (i < (repLen-1)):
				myFile.write( val + ",")
				myCount += 1
			else:
				myFile.write( val + "\n")
				myCount += 1
		# print myCount


def splitOnComma(myStr):
	intArray = []
	tmpString = ""
	iniFlag = 0
	for i in xrange(0,len(myStr)):
		#print myStr[i]
		myVal = myStr[i]
		if ((myVal != ",") and (myVal != "\n")):
			tmpString += myVal
		if ((myVal == ",") and (tmpString != "")):
			myInt = int(tmpString)
			intArray.append(myInt)
			if (iniFlag == 0):
				# print tmpString
				iniFlag = 1
			tmpString = ""
	# print intArray[0]
	return intArray


def externalRequest( input_file, output_file, class_names):
	# print "Hit external request of jGrassAvgAndRep"

	commonFunctions.clearOutputPath(output_file)
	for n in class_names:
		#get files
		fileSet = input_file + n
		dest = output_file + n[:-1] + "Normal.csv"
		repDest = output_file + n[:-1] + "NormalRep.csv"
		myStartingFiles = commonFunctions.getFiles(fileSet)
		#open files
		#normalize files
		normalizedLines = []

		for sample in myStartingFiles:
			fullPath = fileSet + sample
			with open(fullPath, "r") as myFile:
				# print fullPath
				for line in myFile:
					lineStarting = [int(x.strip()) for x in line.split(',')]
					# print len(lineStarting)
					#lineStarting = splitOnComma(line)
					
					lineNormalized = normalizeBySize(lineStarting)
					normalizedLines.append(lineNormalized)
					
					# normalizedLines.append(lineStarting[:])

		#write lines to file
		# print "Writing set to file"
		writeLineToFile( dest, normalizedLines)
		#build a rep(avg) file
		# print "Making Class Rep"
		myRep = makeARep(normalizedLines)
		#print "type of rep after it has been made\t", myRep
		#write normalized files and the rep to the output files

		writeRepToFile( repDest, myRep)

	return 0

def streamlineRequest(input_file, output_file, class_names, make_rep=None):
	# print "Hit new streamline request function"

	commonFunctions.clearOutputPath(output_file)
	myStartingFiles = commonFunctions.getFiles(input_file)

	itemDict = {}
	for q in myStartingFiles:
		for k in class_names:
			if k in q:
				try:
					itemDict[k].append(q)
				except:
					itemDict[k] = [q]
					
	known_classes = itemDict.keys()
	for case in class_names:

		dest = output_file + case + "Normal.csv"
		normalizedLines = []
		if case in known_classes:
			for elt in itemDict[case]:

				fullPath = input_file + elt
				with open(fullPath, "r") as myFile:
					for line in myFile:
						iniLine = [int(x.strip()) for x in line.split(",")]
						lineNormalized = normalizeBySize(iniLine)
						normalizedLines.append(lineNormalized[:])
			if make_rep:
				myRep = makeARep(normalizedLines)
				# print "normalized lines"
				# print normalizedLines
				# print "\n\n"
				# print "rep"
				# print myRep
				normalizedLines.append(myRep[:])
			writeLineToFile( dest, normalizedLines[:])

	return 0


if __name__ == "__main__":
	#set dirs
	mainPath = "/home/socrates/Spring 2015 Bioinformatics Research"
	input_file = mainPath + "/Human/Human Gut NSV/8Mer/"
	output_file = mainPath + "/Human/Normal Gut/8Mer/"



	jGrassClasses = ["Transition/", "Native/", "Invaded/"]
	gutClasses = ["Crohns/", "Healthy/", "UC/"]
	class_names = gutClasses[:]

	for n in class_names:
		#get files
		fileSet = input_file + n
		dest = output_file + n[:-1] + "Normal.csv"
		repDest = output_file + n[:-1] + "NormalRep.csv"
		myStartingFiles = commonFunctions.getFiles(fileSet)
		#open files
		#normalize files
		normalizedLines = []

		for sample in myStartingFiles:
			fullPath = fileSet + sample
			with open(fullPath, "r") as myFile:
				for line in myFile:
					lineStarting = [int(x.strip()) for x in line.split(',')]
					# print len(lineStarting)
					#lineStarting = splitOnComma(line)
					
					lineNormalized = normalizeBySize(lineStarting)
					
					#normalizedLines.append(lineNormalized)
					
					normalizedLines.append(lineNormalized)
		#write lines to file
		print "Writing set to file"
		writeLineToFile( dest, normalizedLines)
		#build a rep(avg) file
		print "Making Class Rep"
		myRep = makeARep(normalizedLines)
		#print "type of rep after it has been made\t", myRep
		#write normalized files and the rep to the output files

		writeRepToFile( repDest, myRep)

