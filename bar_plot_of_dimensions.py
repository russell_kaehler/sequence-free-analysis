"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""
import matplotlib.pyplot as plt
import math 
import argparse
import sys
import shutil
import numpy
import time

import index_to_mer
import commonFunctions


def parse_log_format(log_file):
    dimension_index_count = {}
    num_dims = -1
    total_a_dim_can_show_up = 0
    with open(log_file, 'r') as my_file:
        for line in my_file:
            
            if "\"selected dims\":" in line:
                # print line
                parts = line.split(":")
                dim_str = parts[1].strip()
                dim_str = dim_str[1:-1]
                dim_list = list(dim_str.split(","))
                total_a_dim_can_show_up += 1
                if num_dims == -1:
                    num_dims = len(dim_list)
                else:
                    if num_dims != len(dim_list):
                        sys.exit(1)
                # numeric_list = []
                for i in dim_list:
                    dim_index = int(i)
                    try:
                        dimension_index_count[dim_index] += 1
                    except:
                        dimension_index_count[dim_index] = 1
                # print numeric_list

    return dimension_index_count.copy(), num_dims, total_a_dim_can_show_up


def generate_plot(read_path, write_path, end_name):
    log_files = commonFunctions.get_files_and_path(read_path)
    mer_size_to_selection = {}
    num_trials = {}
    num_dims_used = {}
    total_possible = 0
    mer_size_string =""
    print log_files
    for log in log_files[:]:
        print log
        rhs = log.split("Using_")[1]
        mer_size_string = rhs.split("_")[0]
        # print mer_size
        if mer_size_string not in mer_size_to_selection.keys():
            mer_size_to_selection[mer_size_string] = {}
            num_trials[mer_size_string] = 1
            num_dims_used[mer_size_string] = -1
        else:
            num_trials[mer_size_string] += 1
        dim_count_from_log, n_dims, total_in_run = parse_log_format(log)
        total_possible += total_in_run
        if num_dims_used[mer_size_string] == -1:
            num_dims_used[mer_size_string] = n_dims
        elif num_dims_used[mer_size_string] != n_dims:
            sys.exit(1)
        
        for key in dim_count_from_log.keys():
            try:
                mer_size_to_selection[mer_size_string][key] += dim_count_from_log[key]
            except:
                mer_size_to_selection[mer_size_string][key] = dim_count_from_log[key]
        # print dim_count_from_log

    print "end first for loop pair"
    print mer_size_to_selection
    # print num_trials
    mer_size = int(mer_size_string.split("-")[0])
    for key in mer_size_to_selection.keys():
        print "there were\t",num_trials[key], "\truns of\t",key
        ordered_tuples = [ (v,k) for k,v in mer_size_to_selection[key].iteritems() ]
        ordered_tuples.sort(reverse=True)
        ordered_keys = ["max"]
        ordered_values = [total_possible]
        limit = num_dims_used[key]
        for v,k in ordered_tuples[:limit]:
            mer_str = index_to_mer.external_request(int(k), int(mer_size))
            # print "%d: %d" % (int(k),int(v))
            print str(k)+","+mer_str+","+str(v)
            if int(v) > 1:
                # ordered_keys.append(int(k))
                ordered_keys.append(mer_str)
                ordered_values.append(int(v))

        print ordered_values
        bar_lst = plt.bar(range(len(ordered_values)), ordered_values, align='center')
        # plt.plot(range(len(ordered_values)), ordered_values[::-1], color='red')
        plt.xticks(range(len(ordered_keys)), ordered_keys, rotation=30)
        # labels = ax.get_xticklabels()
        # plt.setp(labels, rotation=30, fontsize=10)
        # popt, pcov = scipy.optimize.curve_fit()
        # numpy.polyfit()
        plt.ylim([0,total_possible])
        bar_lst[0].set_edgecolor("b")
        bar_lst[0].set_facecolor("r")
        # plt.show()

        plt.ylabel('Frequency of Selection')
        plt.xlabel('Dimension Indices')
        plt.tight_layout()
        plt.savefig(write_path + "/" + str(num_trials[key]) + "_trials_on_"+ end_name + ".png")
        plt.close()
    return 0

def crawl_temps(temp_path, selection_string):
    temp_folders = commonFunctions.get_files_and_path(temp_path)
    log_paths = []
    # print "my temp folder path"
    # print temp_folders
    for folder in temp_folders:
        content_files = commonFunctions.get_files_and_path(folder)
        for cnt_file in content_files:
            if "logged" in cnt_file:
                log_paths.append(cnt_file)
    # print log_paths

    log_files = []
    for log_path in log_paths:
        log_homes = commonFunctions.get_files_and_path(log_path)
        for log in log_homes:
            # print "looking at log"
            # print log
            # print "\n"
            if selection_string in log:
                log_files.append(log)

    # for log in log_files:
    #     print log
    # print len(log_files)
    return log_files[:]

def copy_temp_logs_to_storage(log_paths, dest_path):
    #for each item in the log path
    for log in log_paths:
        #copy to the dest path
        temp_side = log.split("Temps/")[1]
        tmp_split = temp_side.split("/logged results/")
        trial_num = tmp_split[0]
        file_name = tmp_split[1]

        # print trial_num
        # print file_name

        final_dest = dest_path + "/" + trial_num + "_" +file_name

        shutil.copy2(log,final_dest)
    return 0

def copy_sequential_runs_to_storage(log_paths, dest_path):
    #for each item in the log path
    counter = 0
    for log in log_paths:
        #copy to the dest path
        # temp_side = log.split("Temps/")[1]
        tmp_split = log.split("/logged results/")
        # trial_num = tmp_split[0]
        file_name = tmp_split[1]

        final_dest = dest_path + "/" + str(counter) + "_" +file_name
        counter += 1
        shutil.copy2(log,final_dest)
    return 0

def read_master_log_folder_find_selected_logs(on_path, selection_string):
    log_list = commonFunctions.get_files_and_path(on_path)
    final_logs = []
    for log in log_list:
        if selection_string in log:
            final_logs.append(log)
    return final_logs

def run_whole_folder():
    plot_out_path = "/home/um/rkaehler/plots"
    log_dest = "/home/um/rkaehler/tempLogs"
    exp_folders = commonFunctions.get_files_and_path(log_dest)
    for experiment in exp_folders:
        experiment_name = experiment.split("/")[-1]
        if experiment_name != "old":
            print experiment
            logs = commonFunctions.get_files_and_path(experiment)[0]
            generate_plot(logs, plot_out_path, experiment_name)
        # print experiment_name

def main():
    parser = argparse.ArgumentParser(prog='Bar plot generation tool')
    parser.add_argument("--read_path", nargs="?", default=None)
    parser.add_argument("--write_path", nargs="?", default=None)

    args = parser.parse_args()
    
    # read_path = "/Users/russkaehler/logged results"
    # k_opts = ["3", "4", "5", "6", "7", "8"]
    # k_names = ["three", "four", "five", "six", "seven", "eight"]

    # for i in xrange(len(k_opts)):

    """on hpc"""
    plot_out_path = "/home/um/rkaehler/plots"
    test_folder = "fiveMer_tenDim_MCCV_KNN"
    # test_folder = k_names[i] + "Mer_tenDim"
    # k_size = k_opts[i] 
    k_size = "5" 
    n_dims = "10"
    # selected_substring = "KNN_new_FS_on_Johnson_Grass_Using_"+ k_size +"-mers_on_"+ n_dims +"-dims"
    # log_dest = "/home/um/rkaehler/tempLogs/knn"+ k_size +"mer"+ n_dims +"dimJGLOOCV" 
    selected_substring = "fiveMer_tenDim_MCCV_KNN"
    log_dest = "/home/um/rkaehler/tempLogs/fiveMer_tenDim_MCCV_KNN"
    temp_path = "/data1/rkaehler/Temps/" + test_folder

    """on socrates"""
    # plot_out_path = "/home/socrates/plots"
    # log_dest = "/home/socrates/tempLogs/HPCknn5mer10dim"
    # master_path = "/home/socrates/logged results"

    # for socrates
    # my_logs = read_master_log_folder_find_selected_logs(master_path, selected_substring)

    # copy_sequential_runs_to_storage(my_logs, log_dest)

    # for the hpc
    my_logs = crawl_temps(temp_path, selected_substring)
    # print my_logs
    copy_temp_logs_to_storage(my_logs, log_dest)

    # generate_plot(args.read_path, args.write_path)
    res = generate_plot(log_dest + "/"+ test_folder, plot_out_path, selected_substring)
    # time.sleep(2)

    return 1

if __name__ == "__main__":
    main()
    # run_whole_folder()
    