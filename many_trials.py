"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student

This is the script to run and evaluate the crossfold tests for
the naive bayes tests of the metagenomic data 
"""

import multiprocessing
import shutil
import datetime
import sys

import processFastaAll
import jGrassAvgAndRep
import jGrassFileMerger
import dataSlicer
import bayes_driver
import commonFunctions
import argparse

import pipeline_functions
import bayes_crossfold

def run(base_path=None, machine_name=None, make_log=0):
    if base_path == None:
        base_path = "/home/"

    if machine_name == None:
        machine_name = "rkaehler"

    main_path = base_path + machine_name

    # johnson_grass_data_path = main_path + "/Johnson Grass Fasta/"
    # /data/rkaehler
    johnson_grass_data_path = "/data/rkaehler/Johnson Grass Fasta/"
    raw_train = main_path + "/JGrass Train/"

    raw_test = main_path + "/JGrass Test/"

    mer_ranges = [3,4,5,6,7,8,9,10,11,12]
    dim_range = [1,3,5,10]

    class_names = ["Transition", "Native", "Invaded"]

    data_set_name = "Johnson_Grass"

    final_matrix = {}

    step_size = 1
    # worker pool (necessary?)

    frag_limit = 400000
    parts = 10

    
    n_bins = 3
    m_est = 0.5

    if make_log == 0:
        date_now = datetime.datetime.now().strftime("%a-%d-%b-%Y_%H:%M:%S")
        log_name = "Bayes_Crossvalidation_on_many_runs_started_at_" + date_now + ".log"

        print log_name
        log_path = main_path + "/trial results/" + log_name
        log_file = open(log_path, "w")

        ini_stdout = sys.stdout
        sys.stdout = log_file

    print "date_now: ", str(date_now)
    print "main_path: ", str(main_path)
    print "step_size: ", str(step_size)

    print "frag_limit: ", str(frag_limit)
    print "parts: ", str(parts)
    
    training_order, test_order = commonFunctions.mix_files(
        johnson_grass_data_path, class_names)

    print log_path

    # if test_size == None or test_size == 0:
    test_size = len(training_order)

    print "test_size: ", str(test_size)

    for index in xrange(test_size):
        commonFunctions.clearOutputPath(raw_train)
        for elt in training_order[index]:
            ini_location = johnson_grass_data_path + elt
            final_location = raw_train + elt
            shutil.copy2(ini_location, final_location)

        commonFunctions.clearOutputPath(raw_test)
        for alt in test_order[index]:
            ini_location = johnson_grass_data_path + alt
            final_location = raw_test + alt
            shutil.copy2(ini_location, final_location)

        for mer_size in mer_ranges:

            print "mer_size: ", str(mer_size)
            
            num_workers = 30
            if mer_size >= 8:
                num_workers = 4

            my_pool = multiprocessing.Pool(num_workers)
            
            pipeline_functions.prep_data(
                mer_size, step_size, frag_limit, parts, my_pool, main_path, class_names)

            my_pool.close()
            my_pool.join()
            for n_dims in dim_range:

                print "n_dims: ", str(n_dims)
                results = bayes_driver.bayes_driver(main_path, n_dims, n_bins, m_est)

    if make_log == 0:
        sys.stdout = ini_stdout
        log_file.close()


def main():

    parser = argparse.ArgumentParser(prog='Bayes Crossvalidation')
    parser.add_argument("--base_path", nargs="?", default=None)
    parser.add_argument("--machine_name", nargs="?", default=None)
    parser.add_argument("--log", nargs="?", default=0)

    args = parser.parse_args()

    base_path = None
    machine_name = None
    log = 0

    if args.base_path:
        base_path = "/" + args.base_path + "/" 

    if args.machine_name:
        machine_name = args.machine_name

    if args.log:
        log = int(args.log)

    run(base_path, machine_name, log)


if __name__ == "__main__":
    main()
