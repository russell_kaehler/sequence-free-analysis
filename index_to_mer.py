"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""


def mer_index_finder(my_string, mer_size):
    # my_string = my_string.lower()
    char_value = {}
    char_value["a"] = 0
    char_value["t"] = 1
    char_value["c"] = 2
    char_value["g"] = 3
    i = 0
    j = 0
    base_four_string = ""

    # myStrLen = len(my_string)
    while(i < mer_size):
        base_four_string += str(char_value[my_string[i]])
        i += 1

    index = int(base_four_string, 4)

    return index

def base_four_int_to_mer(base_four_int):
    num_to_char = {}
    num_to_char["0"] = "a"
    num_to_char["1"] = "t"
    num_to_char["2"] = "c"
    num_to_char["3"] = "g"

    n = 0
    mer_str = ""
    num_str = len(base_four_int)
    while n < num_str:
        mer_str += num_to_char[base_four_int[n]]
        n += 1
    # print mer_str
    return mer_str

def digit_to_char(digit):
    if digit < 10: return chr(ord('0') + digit)
    else: return chr(ord('a') + digit - 10)


def swap_base(index_int, base):
    if index_int < 0:
        return '-' + swap_base(-index_int,base)
    else:
        (d,m) = divmod(index_int,base)
        if d:
            return swap_base(d,base) + digit_to_char(m)
        else:
            return digit_to_char(m)
    # return mer

def external_request(index, mer_size):
    elt = str(swap_base(index, 4))
    i = len(elt)
    while i < mer_size:
            elt = "0" + elt
            # print elt
            i += 1
    mer = base_four_int_to_mer(elt)
    return mer

def main():
    my_lst = [3600, 2535, 2325, 2272, 2072, 1224, 1025, 991, 946, 833, 829]
    mer_size = 6
    for i in my_lst:
        print i
        elt = str(swap_base(i, 4))
        print elt
        while len(elt) < mer_size:
            elt = "0" + elt
        mer = base_four_int_to_mer(elt)
        print mer
        print mer_index_finder(mer, mer_size)
        print "\n"
        print i,",",mer,
    return 0

if __name__ == "__main__":
    main()