"""
Copyright (c)  Russell Kaehler 2016
University of Montana
Computer Science Grad Student

This is the script to run and evaluate the crossfold tests for
the naive bayes tests of the metagenomic data
"""

import multiprocessing
import shutil
import datetime
import sys
import argparse

import bayes_driver
import commonFunctions
import pipeline_functions


def human_data():
    data_path = read_path + "/HumanMunged/"

    raw_train = main_path + "/JGrass Train/"

    raw_test = main_path + "/JGrass Test/"

    class_names = ["Healthy", "UC", "Crohns"]
    class_list = ["0", "1", "2"]
    class_map = {}  # needs to be zip map of names -> numbers
    class_map["hea"] = 0
    class_map["ucn"] = 1
    class_map["cro"] = 2

    data_set_name = "Human_Gut"


def grass_data():
    data_path = read_path + "/Johnson Grass Fasta/"

    raw_train = main_path + "/JGrass Train/"

    raw_test = main_path + "/JGrass Test/"

    class_names = ["Native", "Transition", "Invaded"]
    # class_names = ["Native", "Invaded"]
    class_list = ["0", "1", "2"]
    class_map = {}  # needs to be zip map of names -> numbers
    class_map["nat"] = 0
    class_map["tra"] = 1
    class_map["inv"] = 2

    data_set_name = "Johnson_Grass"


def run_folds(read_path=None, write_path=None, test_size=None, mer_size=None, n_dims=None, num_workers=None, make_log=0):
    """ run the cross fold verification for the naive bayes metagenomic tests"""

    if read_path is None:
        read_path = "/home/rkaehler"

    if write_path is None:
        write_path = "/home/rkaehler"

    # must have starting data set
    main_path = write_path
    data_path = read_path + "/HumanMunged/"

    raw_train = main_path + "/JGrass Train/"

    raw_test = main_path + "/JGrass Test/"

    class_names = ["Healthy", "UC", "Crohns"]
    class_list = ["0", "1", "2"]
    class_map = {}  # needs to be zip map of names -> numbers
    class_map["hea"] = 0
    class_map["ucn"] = 1
    class_map["cro"] = 2

    data_set_name = "Human_Gut"

    date_now = datetime.datetime.now().strftime("%a-%d-%b-%Y_%H:%M:%S")

    final_matrix = {}

    if mer_size is None or mer_size == 0:
        mer_size = 4

    if n_dims is None:
        n_dims = 5
    elif n_dims == 0:
        n_dims = 4 ** mer_size

    step_size = 1
    # worker pool (necessary?)

    frag_limit = 400000
    parts = 10

    n_bins = 3
    m_est = 1
    # workers should be 3x number of parts, 3 classes each with n parts
    if num_workers is None or num_workers == 0:
        num_workers = 3 * parts

    my_pool = multiprocessing.Pool(num_workers)

    print data_set_name
    print mer_size
    print date_now

    if make_log == 0:
        log_name = "BC_on_" + data_set_name + \
            "_Using_" + str(mer_size) + "-mers_on_" + str(n_dims) + \
            "-dims_at_" + date_now + ".log"

        print log_name
        log_path = write_path + "/logged results/" + log_name
        log_file = open(log_path, "w")

        ini_stdout = sys.stdout
        sys.stdout = log_file

    print "\"date_now\":", str(date_now)
    print "\"write_path\":", str(write_path)
    print "\"mer_size\":", str(mer_size)
    print "\"step_size\":", str(step_size)
    # worker pool (necessary?)

    print "\"frag_limit\":", str(frag_limit)
    print "\"parts\":", str(parts)
    print "\"n_dims\":", str(n_dims)
    print "\"n_bins\":", str(n_bins)
    print "\"m_est\":", str(m_est)

    training_order, test_order = commonFunctions.mix_files(
    data_path, class_names)
    # test_order, training_order = commonFunctions.loocv_mix(data_path)

    print training_order
    print "\n\n"
    print test_order

    if test_size is None or test_size == 0:
        test_size = len(training_order)

    print "\"test_size\":", str(test_size)

    for index in xrange(test_size):
        commonFunctions.clearOutputPath(raw_train)
        for elt in training_order[index]:
            ini_location = data_path + elt
            final_location = raw_train + elt
            shutil.copy2(ini_location, final_location)

        commonFunctions.clearOutputPath(raw_test)
        for alt in test_order[index]:
            ini_location = data_path + alt
            final_location = raw_test + alt
            shutil.copy2(ini_location, final_location)

        pipeline_functions.prep_data(
            mer_size, step_size, frag_limit, parts, my_pool, write_path,
            class_names[:], class_map)

        # pipeline_functions.prep_no_slice(
        # mer_size, step_size, my_pool, write_path, class_names[:], class_map)

        results = bayes_driver.bayes_driver(
            write_path, n_dims, n_bins, m_est, class_list[:])

        res_keys = results.keys()
        # print res_keys
        for key in res_keys:
            # print "the key is\t",key
            # print results[key]
            # print type(results[key])
            item_keys = results[key].keys()
            # print item_keys
            for item in item_keys:
                # print item
                # print item,"\t",results[key][item]
                try:
                    final_matrix[key][item] += results[key][item]
                except:
                    try:
                        # final_matrix[key][item].update()
                        final_matrix[key][item] = {}
                        final_matrix[key][item] = results[key][item]

                        # final_matrix[key] = {}
                    except:
                        final_matrix[key] = {}
                        final_matrix[key][item] = {}
                        final_matrix[key][item] = results[key][item]
        print "\"index\":", index
        print "\"running_tally_matrix\":", final_matrix
    print "\"ending_matrix\":", final_matrix
    # print final_matrix

    if make_log == 0:
        sys.stdout = ini_stdout
        log_file.close()

    return 0


def main():

    parser = argparse.ArgumentParser(prog='Bayes Crossvalidation')
    parser.add_argument("--read_path", nargs="?", default=None)
    parser.add_argument("--write_path", nargs="?", default=None)
    parser.add_argument("--num_runs", nargs="?", default=None)
    parser.add_argument("--mer_size", nargs="?", default=None)
    parser.add_argument("--num_workers", nargs="?", default=None)
    parser.add_argument("--n_dims", nargs="?", default=None)
    parser.add_argument("--log", nargs="?", default=0)

    args = parser.parse_args()

    read_path = None
    write_path = None
    num_runs = 0
    mer_size = 0
    n_dims = None
    num_workers = 0
    log = 0

    if args.read_path:
        read_path = "/" + args.read_path + "/"

    if args.write_path:
        write_path = "/" + args.write_path + "/"

    if args.num_runs:
        num_runs = int(args.num_runs)

    if args.mer_size:
        mer_size = int(args.mer_size)

    if args.n_dims:
        n_dims = int(args.n_dims)

    if args.num_workers:
        num_workers = int(args.num_workers)

    if args.log:
        log = int(args.log)

    run_folds(read_path, write_path, num_runs,
              mer_size, n_dims, num_workers, log)

if __name__ == "__main__":
    main()
