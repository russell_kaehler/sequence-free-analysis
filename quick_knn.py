"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""

import multiprocessing
import shutil
import datetime
import sys
import argparse
import numpy as np
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

import processFastaAll
import jGrassAvgAndRep
import jGrassFileMerger
import dataSlicer
# import bayes_driver
import naive_bayes
import commonFunctions
import argparse
import bayes_crossfold


def run_folds(read_path=None, write_path=None, test_size=None, mer_size=None, n_dims=None, num_workers=None, make_log=1):
    """ run the cross fold verification for the knn metagenomic tests"""

    if read_path is None:
        read_path = "/home/rkaehler"

    if write_path is None:
        write_path = "/home/rkaehler"

    # must have starting data set
    main_path = write_path

    johnson_grass_data_path = read_path + "/Johnson Grass Fasta/"

    raw_train = main_path + "/JGrass Train/"

    raw_test = main_path + "/JGrass Test/"

    class_names = ["Transition", "Native", "Invaded"]

    # class_list = ["0", "1", "2"]
    class_map = {}  # needs to be zip map of names -> numbers
    class_map["nat"] = 0
    class_map["tra"] = 1
    class_map["inv"] = 2

    data_set_name = "Johnson_Grass"

    date_now = datetime.datetime.now().strftime("%a-%d-%b-%Y_%H:%M:%S")

    final_matrix = {}

    if mer_size is None or mer_size == 0:
        mer_size = 4

    if n_dims is None:
        n_dims = 5
    elif n_dims == 0:
        n_dims = 4 ** mer_size

    step_size = 1
    # worker pool (necessary?)

    frag_limit = 400000
    parts = 10

    
    n_bins = 3
    m_est = 1

    k_size = 1
    # workers should be 3x number of parts, 3 classes each with n parts
    if num_workers is None or num_workers == 0:
        num_workers = 3 * parts

    my_pool = multiprocessing.Pool(num_workers)

    print data_set_name
    print mer_size
    print date_now

    if make_log == 0:
        log_name = "KNN_on_" + data_set_name + \
            "_Using_" + str(mer_size) + "-mers_on_" + str(n_dims) + \
            "-dims_at_" + date_now + ".log"

        print log_name
        log_path = main_path + "/logged results/" + log_name
        log_file = open(log_path, "w")

        ini_stdout = sys.stdout
        sys.stdout = log_file

    print "\"date_now\":", str(date_now)
    print "\"main_path\":", str(main_path)
    print "\"mer_size\":", str(mer_size)
    print "\"step_size\":", str(step_size)
    # worker pool (necessary?)

    print "\"frag_limit\":", str(frag_limit)
    print "\"parts\":", str(parts)
    print "\"n_dims\":", str(n_dims)
    print "\"n_bins\":", str(n_bins)
    print "\"m_est\":", str(m_est)

    # training_order, test_order = commonFunctions.mix_files(
    #     johnson_grass_data_path, class_names)


    # for elt in training_order[index]:
    #     ini_location = johnson_grass_data_path + elt
    #     final_location = raw_train + elt
    #     shutil.copy2(ini_location, final_location)

    # commonFunctions.clearOutputPath(raw_test)
    # for alt in test_order[index]:
    #     ini_location = johnson_grass_data_path + alt
    #     final_location = raw_test + alt
    #     shutil.copy2(ini_location, final_location)

    # pipeline_functions.prep_data(
    #     mer_size, step_size, frag_limit, parts, my_pool, main_path, class_names, class_map)

    #results = bayes_driver.bayes_driver(main_path, n_dims, n_bins, m_est)
    results = external_request( main_path, k_size, n_dims)
    res_keys = results.keys()
    # print res_keys
    for key in res_keys:
        # print "the key is\t",key
        # print results[key]
        # print type(results[key])
        item_keys = results[key].keys()
        # print item_keys
        for item in item_keys:
            # print item
            # print item,"\t",results[key][item]
            try:
                final_matrix[key][item] += results[key][item]
            except:
                try:
                    # final_matrix[key][item].update()
                    final_matrix[key][item] = {}
                    final_matrix[key][item] = results[key][item]

                    # final_matrix[key] = {}
                except:
                    final_matrix[key] = {}
                    final_matrix[key][item] = {}
                    final_matrix[key][item] = results[key][item]
    print "\"index\":", index
    print "\"running_tally_matrix\":", final_matrix

    return 0



def select_dims_old(data_in, n_dims):
    """ use a t-test to find the top n dims in a data set"""
    # print "getting t-test scores"
    my_t_scores = metagenomicKNN.find_T_score(data_in[:])
    # print "found t-test scores"
    
    # print "\n"
    the_best_dims = commonFunctions.find_best_n_dims_from_t_test_results( n_dims, my_t_scores[:])
    print "\"selected dims\":" + str(the_best_dims)
    # print the_best_dims

    return the_best_dims


def select_dims(data_in, n_dims):
    """ use a t-test to find the top n dims in a data set"""

    my_array = np.array(data_in,dtype=float)
    data_part = my_array[:,:-1]
    print data_part
    class_part = my_array[:,-1]
    print class_part
    X_res = SelectKBest(chi2, k=n_dims).fit(data_part, class_part)
    print X_res.get_support()
    the_best_dims = np.where(X_res.get_support().tolist())
    print the_best_dims[0].tolist()

    return the_best_dims[0].tolist()

 
def external_request( main_path, votingSize, n_dims):
    """ call this function to run the driver """

    final_train = main_path + "/Final Train/"
    final_test = main_path + "/Final Test/"

    #input for metagenomic processing
    final_train_output = final_train + "trainingNormalized.csv"
    final_test_output = final_test + "testingNormalized.csv"

    training_data = commonFunctions.data_parser(final_train_output)
    test_data = commonFunctions.data_parser(final_test_output)

    training_data = commonFunctions.make_data_numeric(training_data[:])
    test_data = commonFunctions.make_data_numeric(test_data[:])

    # theTen = select_dims(training_data, n_dims)
    
    if n_dims < (len(test_data[0]) - 1):
        print "selecting dims"
        use_dims = select_dims(training_data[:], n_dims)

        training_data = commonFunctions.pull_subset_of_data_keep_class_vect(use_dims[:], training_data[:])

        test_data = commonFunctions.pull_subset_of_data_keep_class_vect( use_dims[:], test_data[:])
    else:
        print "using all dims"
    # print "\n\n\n"


def main():

    parser = argparse.ArgumentParser(prog='Multi Crossvalidation')
    parser.add_argument("--read_path", nargs="?", default=None)
    parser.add_argument("--write_path", nargs="?", default=None)
    parser.add_argument("--num_runs", nargs="?", default=None)
    parser.add_argument("--mer_size", nargs="?", default=None)
    parser.add_argument("--num_workers", nargs="?", default=None)
    parser.add_argument("--n_dims", nargs="?", default=None)
    parser.add_argument("--log", nargs="?", default=0)

    args = parser.parse_args()

    read_path = None
    write_path = None
    num_runs = 0
    mer_size = 0
    n_dims = None
    num_workers = 0
    log = 1

    if args.read_path:
        read_path = "/" + args.read_path + "/"

    if args.write_path:
        write_path = "/" + args.write_path + "/"

    if args.num_runs:
        num_runs = int(args.num_runs)

    if args.mer_size:
        mer_size = int(args.mer_size)

    if args.n_dims:
        n_dims = int(args.n_dims)

    if args.num_workers:
        num_workers = int(args.num_workers)

    if args.log:
        log = int(args.log)

    run_folds(read_path, write_path, num_runs,
              mer_size, n_dims, num_workers, log)

if __name__ == "__main__":
    main()