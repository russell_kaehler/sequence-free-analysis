"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""
import numpy as np
import scipy
import matplotlib.pyplot as plt

import commonFunctions
import heat_map_maker


def main(log_path):

    my_logs = commonFunctions.get_files_and_path(log_path)
    num_logs = len(my_logs)
    my_array = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    for log in my_logs:
        with open(log, "rb") as log:
            for line in log:
                if "\"ending_matrix\":" in line:
                    final_line = line.split("\"ending_matrix\":")[1].strip()
                    # print final_line
                    final_as_dict = eval(final_line)
                    # print final_as_dict
                    # print type(final_line),"\t",type(final_as_dict)
                    # print final_as_dict["TransitionNormal.csv"]
                    for key in final_as_dict.keys():
                        for guess in final_as_dict[key].keys():
                            guess_int = int(guess)
                            if key == "NativeNormal.csv":
                                my_array[0][
                                    guess_int] += final_as_dict[key][guess]
                            if key == "TransitionNormal.csv":
                                my_array[1][
                                    guess_int] += final_as_dict[key][guess]
                            elif key == "InvadedNormal.csv":
                                my_array[2][
                                    guess_int] += final_as_dict[key][guess]

    print my_array[0]
    print my_array[1]
    print my_array[2]

    total_right = my_array[0][0] + my_array[1][1] + my_array[2][2]
    total = sum(my_array[0]) + sum(my_array[1]) + sum(my_array[2])
    print total_right
    print total
    print "run total is\t", total_right / (total * 1.0)
    return my_array, num_logs


if __name__ == "__main__":
    # temp_log_path = "/home/socrates/tempLogs/nb5mer5dimJGKNN/KNN_fiveMer_fiveDim"
    temp_log_path = "/home/um/rkaehler/tempLogs/fiveMer_tenDim_MCCV_KNN/fiveMer_tenDim_MCCV_KNN"
    results_matrix, num_trials = main(temp_log_path)
    # results_matrix = [[results_matrix[0][0], results_matrix[0][2]], [
    #     results_matrix[2][0], results_matrix[2][2]]]
    my_array = np.array(results_matrix)
    # class_names = ["Healthy", "UC", "Crohns"]
    class_names = ["Native", "Transition", "Invaded"]
    # class_names = ["Healthy", "Crohns"]
    # plot_out_path = "/home/socrates/plots"
    plot_out_path = "/home/um/rkaehler/plots"
    my_title = "KNN_Confusion_Matrix_5-Mers_10-Dimensions_%s-JG_Data_with_Normalization" % (
        str(num_trials))
    print my_title
    # std_error = scipy.stats.sem(my_array)
    # std_dev = np.std(my_array)
    # print "std error: ", std_error
    # print "std dev: ", std_dev
    plt.figure()
    heat_map_maker.plot_confusion_matrix(my_array, class_names)
    plt.savefig(plot_out_path + "/" + my_title + ".png")
