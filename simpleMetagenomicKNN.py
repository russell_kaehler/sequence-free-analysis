import math

"""
------ get data from file, needs labels already know!
"""
def data_parser( pathToFile):
	
	#build list of data records
	myDataSet = []
	myClassSet = []
	#open file read each line removing \r\n and place that into a list of lines
	with open( pathToFile, 'r') as myFile:
		#loop over lines in file
		for line in myFile:
			#read line ~lr to above
			currentLine = line.replace('\r\n','').strip()
			tempList = []
			#if line has more than one split
			if len(currentLine.split(',')) > 1:
				#loop over what the split fn returns
				for value in currentLine.split(','):
					#add them values to your temp list
					tempList.append(value)
				#do the python thing and zip labels in a 1-1 mapping to your temp list
				#use that as a mapping for a dict and add that dict to your list!
				# B A M ! 
				myClassSet.append(tempList[-1])
				del(tempList[-1])
				myDataSet.append(tempList)
	#remove the line with labels

	
	#build data dictionary of lines with label value pairs
			
	#delete record where label maps to label - boring! - - also confuses classifier, if not done = (
	#del myDataSet[0]
	return myDataSet, myClassSet
	
def find_Manhattan( pointA, pointB):
	dist = 0
	#print pointA
	for x in range(0,len(pointA)):
		dist += math.fabs(float(pointA[x]) - float(pointB[x]))
	return dist

#data file
startingFile = "normalJohnsonGrassDataWithClassVect.csv"

rawData, dataClasses = data_parser( startingFile)

print rawData[0][0:10]
del rawData[0]
print rawData[0][0:10]
print dataClasses,"\tdata classes"
del dataClasses[0]

distAB = []
for elt in range(0,len(rawData)):
	tempList = []
	for alt in range(0, len(rawData)):
		if alt == elt:
			tempList.append(0)
		else:
			dist = find_Manhattan(rawData[elt], rawData[alt])
			tempList.append(dist)
	distAB.append(tempList)

for i in distAB:
	print i

winners = []
votingSize = 1
for i in range(0,len(distAB)):
	myList = distAB[i]
	#print i
	tempDict = {}
	for j in range(0,len(distAB[i])):
		if j != i:
			ijDist = distAB[i][j]
			
			if len(tempDict.keys()) < votingSize:
				tempDict[j] = ijDist
			else:
				maxValue = max(tempDict.values())
				#print maxValue
				valueRow = -1
				for key in tempDict.keys():
					if maxValue == tempDict[key]:
						valueRow = key
						
				if (ijDist < maxValue) and (valueRow != -1):
					del(tempDict[valueRow])
					tempDict[j] = ijDist
	winners.append(tempDict) 

results = [[0,0,0],[0,0,0],[0,0,0]]
print results
	
for i in range(1, len(winners)):
	rowDict = winners[i]

	rowKeys = rowDict.keys()
	itemClass = dataClasses[i]
	votes = {}
	for x in range( 0, len(rowKeys)):
		rowClass = dataClasses[rowKeys[x]]
		if rowClass not in votes.keys():
			votes[rowClass] = 1
		else:
			votes[rowClass] += 1
			
	maxVotes = -1
	predClass = -1
	for x in votes.keys():
		if maxVotes < votes[x]:
			maxVotes = votes[x]
			predClass = x
	print "my item class\t", itemClass
	print "my pred class\t", predClass,"\t with \t",maxVotes,"\t in favor"
	results[int(itemClass)][int(predClass)] += 1
	print i,"\t",itemClass,"\t",predClass
		
print results








