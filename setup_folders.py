"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Comuter Science Grad Student

This is the script to run and evaluate the crossfold tests for
the naive bayes tests of the metagenomic data 
"""
import os
import argparse

import commonFunctions

def hpc_build(base_path):
    folder_list = []
    folder_list.append(base_path + "/JGrass Train/")

    folder_list.append(base_path + "/JGrass Test/")

    folder_list.append(base_path + "/logged results/")

    folder_list.append(base_path + "/JGrass Train/")

    folder_list.append(base_path + "/JGrass Test/")

    folder_list.append(base_path + "/Limited Train/")

    folder_list.append(base_path + "/Limited Test/")

    # input fasta is used by the process fasta script
    folder_list.append(base_path + "/Train Splits/")
    folder_list.append(base_path + "/Test Splits/")
    # this is used as the output for process fasta output
    # also used as the input for avg and rep
    folder_list.append(base_path + "/NSV Train/")
    folder_list.append(base_path + "/NSV Test/")
    # output for avg and rep
    # input for file merger
    folder_list.append(base_path + "/Normalized Split/")

    folder_list.append(base_path + "/Normalized Two/")

    folder_list.append(base_path + "/Final Train/")
    folder_list.append(base_path + "/Final Test/")

    # folder_list.append(base_path + "/trial results/")

    commonFunctions.build_folder_list(folder_list[:])
    return folder_list[:]




def main(machine_name=None, base_path=None):
    
    if machine_name == None:
        machine_name = "rkaehler"

    if base_path == None:
        base_path = "/home/"
    # must have starting data set

    folders = []

    main_path =  base_path + machine_name

    # folders.append(main_path + "/Johnson Grass Fasta/")

    folders.append(main_path + "/JGrass Train/")

    folders.append(main_path + "/JGrass Test/")

    folders.append(main_path +"/logged results/")

    folders.append(main_path + "/JGrass Train/")

    folders.append(main_path + "/JGrass Test/")

    folders.append(main_path + "/Limited Train/")

    folders.append(main_path + "/Limited Test/")

    # input fasta is used by the process fasta script
    folders.append(main_path + "/Train Splits/")
    folders.append(main_path + "/Test Splits/")
    # this is used as the output for process fasta output
    # also used as the input for avg and rep
    folders.append(main_path + "/NSV Train/")
    folders.append(main_path + "/NSV Test/")
    # output for avg and rep
    # input for file merger
    folders.append(main_path + "/Normalized Split/")

    folders.append(main_path + "/Normalized Two/")

    folders.append(main_path + "/Final Train/")
    folders.append(main_path + "/Final Test/")

    folders.append(main_path + "/trial results/")

    for path in folders:
        if not os.path.exists(path):
            os.makedirs(path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='check and setup folders')
    parser.add_argument("--machine_name", nargs="?", default=None)
    parser.add_argument("--base_path", nargs="?", default=None)

    args = parser.parse_args()

    machine_name = None
    base_path = None

    if args.machine_name:
        machine_name = args.machine_name
    if args.base_path:
        base_path = args.base_path

    main(machine_name, base_path)
