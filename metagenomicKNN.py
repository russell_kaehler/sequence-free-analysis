"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""

import math
import scipy
from scipy import stats

import commonFunctions


def data_parser( pathToFile):
	
	#build list of data records
	fullData = []
	#open file read each line removing \r\n and place that into a list of lines
	with open( pathToFile, 'rb') as myFile:
		#loop over lines in file
		for line in myFile:
			#read line ~lr to above
			currentLine = line.replace('\r\n','').strip()
			tempList = []
			#if line has more than one split
			if len(currentLine.split(',')) > 1:
				#loop over what the split fn returns
				for value in currentLine.split(','):
					#add the values to your temp list
					tempList.append(value)

				fullData.append(tempList[:])
	return fullData


def find_Manhattan( point_a, point_b):
	dist = 0
	for x in range(0,len(point_a)):
		dist += math.fabs(float(point_a[x]) - float(point_b[x]))
	return dist


def find_ANOVA( dataSubset):
	fScores = []
	pValues = []
	dataClasses = []
	if len(dataSet) > 0:
		#need to subset the data by class
		a = 1
	return 1


#t-test is an all pairwise comparison (upper triangular matrix)
def find_T_score( dataSet):
	tScores = []
	pValues = []
	# newData = []
	if len(dataSet) > 0:
		#loop over cols make a transposed data set
		col_len = len(dataSet[0][:-1])
		row_len = len(dataSet)
		for col in range(0, col_len):
			subSetHolder = {}
			#for levels of classes
			#take indicies and transform them into subsets of the data
			for i in range(0, row_len):
				classI = dataSet[i][-1]
				#print classI,"\t",subSetHolder.keys()
				if classI in subSetHolder.keys():

					subSetHolder[classI].append(float(dataSet[i][col]))
				else:
					nuList = [float(dataSet[i][col])]
					subSetHolder[classI] = nuList

			myKeys = sorted(subSetHolder.keys())
			count = 1.0
			tmpT = 0.0
			tmpP = 0.0
			key_len = len(myKeys)
			for x in range(0, key_len-1):
				xData = subSetHolder[myKeys[x]]
				for y in range(x+1, key_len):
					yData = subSetHolder[myKeys[y]]

					tRes, pVal = stats.ttest_ind(xData, yData)

					tRes = math.fabs(tRes)
					pVal = math.fabs(pVal)
					tmpT += tRes
					tmpP += pVal
				count += 1.0
				
			myColT = tmpT / count
			myColP = tmpP / count
			
			tScores.append(myColT)
			pValues.append(myColP)

	return tScores
	

def writeOut( outputName, collectionOfData):
	"""	
	outFileName = "8MerSubsetHumanGut.csv"
	with open(outFileName, "w") as myOut:
		for row in range(0,len(dataSubset)):
			for col in range(0,len(dataSubset[0])):
					myOut.write(dataSubset[row][col] + ",")
			myOut.write(fullResults[row][-1] + "\n")
	"""
	return 0


def makeDistList(dataIn, cRow):
	distAB = {}

	for alt in range(0, len(dataIn)):
		if alt != cRow:
			dist = find_Manhattan(dataIn[cRow], dataIn[alt])
			distAB[alt] =dist
	return distAB.copy()


def findBestNDims( n, tResults):
	myTen = {}
	for i in range(0,len(tResults)):
		val = tResults[i]
		if len(myTen) < n:
			myTen[val] = i
		else:
			minValue = min(myTen)
			minLocation = myTen[minValue]
			#minLocation = myTScores[pseudoKey]
			#print val, "\t", minValue
			if val > minValue:
				del(myTen[minValue])
				myTen[val] = i
	return myTen.copy()


def pullSubsetOfData(winningDims, dataIn):
	dataSubset = []
	keyList = winningDims.keys()
	for x in range(0,len(keyList)):
		col = winningDims[keyList[x]]
		for row in range(0,len(dataIn)):
			if x == 0:
				dataSubset.append([])
			val = dataIn[row][col]
			dataSubset[row].append(val)
	return dataSubset		
	

def externalRequest( dataSet):
	# print "Hit external request of knn system"

	fullResults = data_parser( dataSet)

	# print "\n\n\n"
	
	#TODO t-test can be done in parallel
	
	#TODO move t-test into block
	#change range vals to fullResults above (can keep index)
	winners = []
	votingSize = 1
	for i in range(0,len(fullResults)):
		#myList = fullResults[i]
		#print i
		cData = fullResults[:i] + fullResults[i+1:]
		myTScores, myPValues = find_T_score( cData)
		
		#get top ten dimensions
		#TODO: change from hard 10 dims to N dims
		theTen = findBestNDims( 10, myTScores[:])
		dataSubset = pullSubsetOfData(theTen, fullResults[:])
		
		distAB = makeDistList(dataSubset, i)
		tempDict = {}
		for j in distAB.keys():
			#TODO change s.t. t-test, dim selection, dist calc all fall here
			#then vote

			ijDist = distAB[j]
		
			if len(tempDict.keys()) < votingSize:
				tempDict[j] = ijDist
			else:
				maxValue = max(tempDict.values())
				#print maxValue
				valueRow = -1
				for key in tempDict.keys():
					if maxValue == tempDict[key]:
						valueRow = key
					
				if (ijDist < maxValue) and (valueRow != -1):
					del(tempDict[valueRow])
					tempDict[j] = ijDist
		winners.append(tempDict) 

	results = [[0,0,0],[0,0,0],[0,0,0]]
	print results

	for i in range(0, len(winners)):
		rowDict = winners[i]

		rowKeys = rowDict.keys()
		itemClass = fullResults[i][-1]
		votes = {}
		for x in range( 0, len(rowKeys)):
			rowClass = fullResults[rowKeys[x]][-1]
			if rowClass not in votes.keys():
				votes[rowClass] = 1
			else:
				votes[rowClass] += 1
			
		maxVotes = -1
		predClass = -1
		for x in votes.keys():
			if maxVotes < votes[x]:
				maxVotes = votes[x]
				predClass = x
		results[int(itemClass)][int(predClass)] += 1
		print i,"\t",itemClass,"\t",predClass
		
	print results

	return 1

if __name__ == "__main__":
	#data file
	startingFile = "JohnsonGrassDataWithClassVectWith8-Mers at time2015-10-04 15:26:25.522622.csv"

	fullResults = data_parser( startingFile)

	print "\n\n\n"
	
	#TODO t-test can be done in parallel
	

	#TODO move t-test into block
	#change range vals to fullResults above (can keep index)
	winners = []
	votingSize = 1
	for i in range(0,len(fullResults)):
		#myList = fullResults[i]
		#print i
		cData = fullResults[:i] + fullResults[i+1:]
		myTScores, myPValues = find_T_score( cData)
		
		#get top ten dimensions
		theTen = findBestNDims( 10, myTScores[:])
		dataSubset = pullSubsetOfData(theTen, fullResults[:])
		
		distAB = makeDistList(dataSubset, i)
		tempDict = {}
		for j in distAB.keys():
			#TODO change s.t. t-test, dim selection, dist calc all fall here
			#then vote

			ijDist = distAB[j]
		
			if len(tempDict.keys()) < votingSize:
				tempDict[j] = ijDist
			else:
				maxValue = max(tempDict.values())
				#print maxValue
				valueRow = -1
				for key in tempDict.keys():
					if maxValue == tempDict[key]:
						valueRow = key
					
				if (ijDist < maxValue) and (valueRow != -1):
					del(tempDict[valueRow])
					tempDict[j] = ijDist
		winners.append(tempDict) 



	results = [[0,0,0],[0,0,0],[0,0,0]]
	print results

	
	for i in range(0, len(winners)):
		rowDict = winners[i]

		rowKeys = rowDict.keys()
		itemClass = fullResults[i][-1]
		votes = {}
		for x in range( 0, len(rowKeys)):
			rowClass = fullResults[rowKeys[x]][-1]
			if rowClass not in votes.keys():
				votes[rowClass] = 1
			else:
				votes[rowClass] += 1
			
		maxVotes = -1
		predClass = -1
		for x in votes.keys():
			if maxVotes < votes[x]:
				maxVotes = votes[x]
				predClass = x
		results[int(itemClass)][int(predClass)] += 1
		print i,"\t",itemClass,"\t",predClass
		
	print results
	
	
