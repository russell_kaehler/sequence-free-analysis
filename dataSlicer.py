"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""

import os
import sys
import time
import random

import commonFunctions
#take all classes

#for each class get 100k records (random from all elements) (how many total reads do I have)

#randomly split the mega file into n smaller, non overlapping mini files

#call the newly created file the sample

#this will allow for a 10 x 10 crossfold sampling methodology 

def checkProcessedDataExists(exitPath):
  exitFiles = commonFunctions.getFiles(exitPath)

  # print "this is the list of exit files\t", exitFiles
  if len(exitFiles) > 0:
      rmRes = commonFunctions.removeExisitingData(exitPath)
      if rmRes != 0:
        sys.exit(99)

  return 0


#split files by class, build one 'mega file' per class
def makeSampleMegaFiles(exitPath, classes):

  epoch_time = str(int(time.time()))

  finalFiles = {}

  for i in classes:
    elt_path = exitPath + i + "-"+ epoch_time + ".txt"
    with open(elt_path, "w") as myFile:
      myFile.write("")
    finalFiles[i] = elt_path

  return finalFiles


def sortContentsSaveFrags(input, output):
  tmpFrags = []
  with open(input, "r") as myFile:
    for line in myFile:
      tmpFrags.append(line)

  with open(output, "a") as nxtFile:
    for frag in tmpFrags:
      nxtFile.write(frag)

  return 0


def chunkIt(seq, num):
  avg = len(seq) / float(num)
  out = []
  last = 0.0

  while last < len(seq):
    out.append(seq[int(last):int(last + avg)])
    last += avg

  return out


def read_mega_file_and_split(inputPath, outputPath, limit, parts):
  mega_files = commonFunctions.getFiles(inputPath)
  commonFunctions.removeExisitingData(outputPath)
  for mega in mega_files:
    tag = ""
    tagCounts = 0
    frag = ""
    fragCounts = 0
    tagFragDict = {}

    hyphen_location = mega.rfind("-")
    mega_class = mega[:hyphen_location]

    fileString = inputPath + mega
    # print fileString
    with open(fileString, "r") as myFile:
      # print "inside with"
      for line in myFile:
        # print line
        if( line[:1] == ">"):
          tag = line
          tagCounts += 1
        if( line[:1] != ">"):
          frag = line
          fragCounts += 1
        if( (frag != "") and (tag != "")):
          tagFragDict[tag] = frag
          tag = ""
          frag = ""
    # find number of frags    
    keys = tagFragDict.keys()

    #split frags into n equal parts
    random.shuffle( keys)
    if limit:
      keys = keys[:limit]
    # print len(keys)
    splits = chunkIt(keys, parts)
    cnt = 0
    for split in splits:
      # print len(split)
      last_name = outputPath + mega_class + "-split-"  + str(cnt) + ".fasta"
      cnt += 1
      with open(last_name, "w") as out_file:
        for index in split:
          out_file.write(index + tagFragDict[index])

  return 1


def externalRequest(iniPath, exitPath, classes):
  iniFiles = commonFunctions.getFiles(iniPath)
  if checkProcessedDataExists(exitPath) == 0:
    byClass  = []

    finalFiles = makeSampleMegaFiles(exitPath, classes)
    # print finalFiles
    for elt in iniFiles:
      for name in classes:
        if name in elt:
          #append reads from file into megafile
          res = sortContentsSaveFrags(iniPath+elt, finalFiles[name])

  # print "done"
  return 0


def main(iniPath, exitPath, splitPath, classes, my_limit, parts):
  # print "in main of data slicer"
  # parts = 10
  cPDE = checkProcessedDataExists(exitPath)

  finalFiles = makeSampleMegaFiles(exitPath, classes)

  externalRequest(iniPath, exitPath, classes)
  
  read_mega_file_and_split(exitPath, splitPath, my_limit, parts)
  return 0


if __name__ == "__main__":

  machineName = "rkaehler"

  mainPath = "/home/" + machineName + "/Spring 2015 Bioinformatics Research"

  my_limit = 1000000
  parts = 10

  # sampleType = "Human"
  # sampleType = "Johnson Grass Data"
  # sampleType = "Random"
  sampleType = "saliva"
  # sampleType = "confusing saliva"
  if sampleType == "Human":
    iniPath = mainPath + "/" + sampleType + "/Fasta Files/"
    exitPath = "/home/rkaehler/Human Data 2016/"

    splitPath = "/home/" + machineName + "/Temp Splits/"
    humanClasses = ["Healthy", "Crohns", "UC"]
    
    main(iniPath, exitPath, splitPath, humanClasses, my_limit, parts)
  if sampleType == "Johnson Grass Data":
    iniPath = mainPath + "/" + sampleType + "/Johnson Grass Fasta/"
    exitPath = "/home/rkaehler/Johnson Data 2016/"

    splitPath = "/home/" + machineName + "/Temp Splits/"
    jGrassClasses = ["Transition", "Native", "Invaded"]
    
    main(iniPath, exitPath, splitPath, jGrassClasses, my_limit, parts)

  if sampleType == "Random":
    iniPath = "/home/rkaehler/Random Fasta/"
    exitPath = "/home/rkaehler/Random 2016/"
    splitPath = "/home/" + machineName + "/Temp Splits/"
    rClasses = ["Wolves", "Lions", "Bears"]
    main(iniPath, exitPath, splitPath, rClasses, my_limit, parts)

  if sampleType == "saliva":
    my_limit = 240000
    iniPath = "/home/rkaehler/salivary data/"
    exitPath = "/home/rkaehler/Split Saliva/"
    splitPath = "/home/" + machineName + "/Temp Splits/"
    rClasses = ["102", "103", "943"]
    main(iniPath, exitPath, splitPath, rClasses, my_limit, parts)


  if sampleType == "confusing saliva":
    my_limit = 70000
    iniPath = "/home/rkaehler/confusing saliva/"
    exitPath = "/home/rkaehler/Split Saliva/"
    splitPath = "/home/" + machineName + "/Temp Splits/"
    rClasses = ["192", "970", "971"]
    main(iniPath, exitPath, splitPath, rClasses, my_limit, parts)


