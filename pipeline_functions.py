"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student

This is the script to run and evaluate the crossfold tests for
the naive bayes tests of the metagenomic data 
"""

import multiprocessing
import shutil
import datetime
import sys
import time

import processFastaAll
import jGrassAvgAndRep
import jGrassFileMerger
import dataSlicer
import bayes_driver
import commonFunctions
import argparse


def prep_data(mer_size, step_size, frag_limit, parts, my_pool, main_path, class_names, class_map):
    """ prepare data after files have been moved """
    # print "Call has been made to run script"
    # print "calling split"
    slice_result = slice_data(main_path, class_names, frag_limit, parts)
    process_results = process_fasta(main_path, my_pool, mer_size, step_size)
    average_results = average_and_merge(main_path, class_names, class_map, mer_size)


def prep_no_slice(mer_size, step_size, my_pool, main_path, class_names, class_map):
    """ prepare data after files have been moved do not split and pool """
    process_results = process_fasta_no_split(main_path, my_pool, mer_size, step_size)
    average_results = average_and_merge(main_path, class_names, class_map, mer_size)


def slice_data(main_path, class_names, frag_limit, parts):
    """ use data slicer and split test and training files into smaller
    bootstrapped files pulling fragments without replacement from the initial files"""
    raw_train = main_path + "/JGrass Train/"

    raw_test = main_path + "/JGrass Test/"


    limited_train = main_path + "/Limited Train/"

    limited_test = main_path + "/Limited Test/"

    # input fasta is used by the process fasta script
    input_train = main_path + "/Train Splits/"
    input_test = main_path + "/Test Splits/"

    dataSlicer.main(raw_train, limited_train, input_train,
                    class_names, frag_limit, parts)

    dataSlicer.main(raw_test, limited_test, input_test,
                    class_names, frag_limit, parts)

    return 0


def process_fasta(main_path, my_pool, mer_size, step_size):
    # input fasta is used by the process fasta script
    input_train = main_path + "/Train Splits/"
    input_test = main_path + "/Test Splits/"
    # #call initial fasta to nsv all file

    # this is used as the output for process fasta output
    # also used as the input for avg and rep
    nsv_train = main_path + "/NSV Train/"
    nsv_test = main_path + "/NSV Test/"

    processFastaAll.externalRequest(
        input_train, nsv_train, my_pool, mer_size, step_size)
    
    processFastaAll.externalRequest(
        input_test, nsv_test, my_pool, mer_size, step_size)

    return 0

def process_fasta_no_split(main_path, my_pool, mer_size, step_size):
    # input fasta is used by the process fasta script
    input_train = main_path + "/JGrass Train/"
    input_test = main_path + "/JGrass Test/"
    # #call initial fasta to nsv all file

    # this is used as the output for process fasta output
    # also used as the input for avg and rep
    nsv_train = main_path + "/NSV Train/"
    nsv_test = main_path + "/NSV Test/"

    processFastaAll.externalRequest(
        input_train, nsv_train, my_pool, mer_size, step_size)
    
    processFastaAll.externalRequest(
        input_test, nsv_test, my_pool, mer_size, step_size)

    return 0


def average_and_merge(main_path, class_names, class_map, mer_size):
    train_status = average_and_merge_train(main_path[:], class_names[:], class_map, mer_size)
    test_status = average_and_merge_test(main_path[:], class_names[:], mer_size)

    return 0


def average_and_merge_train(main_path, class_names, class_map, mer_size):
    # #average samples and produce a rep
    nsv_train = main_path + "/NSV Train/"

    # output for avg and rep
    # input for file merger
    normalized_train = main_path + "/Normalized Split/"

    final_train = main_path + "/Final Train/"

    # input for metagenomic processing
    final_train_output = final_train + "trainingNormalized.csv"

    jGrassAvgAndRep.streamlineRequest(nsv_train, normalized_train, class_names, make_rep=True)

    jGrassFileMerger.externalRequest_by_class(
        normalized_train, final_train, final_train_output, mer_size, class_map)
    return 0


def average_and_merge_test(main_path, class_names, mer_size):

    nsv_test = main_path + "/NSV Test/"

    normalized_test = main_path + "/Normalized Two/"

    final_test = main_path + "/Final Test/"

    # input for metagenomic processing
    final_test_output = final_test + "testingNormalized.csv"

    jGrassAvgAndRep.streamlineRequest(nsv_test, normalized_test, class_names)

    jGrassFileMerger.externalRequest_by_filename(
        normalized_test, final_test, final_test_output, mer_size)
    return 0
