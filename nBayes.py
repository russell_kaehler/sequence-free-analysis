
import commonFunctions
import metagenomicKNN


#naieve bayes should be added to the platform

#return a matrix from the data set such that all 
#cols that aren't a class indicator we know the max/mix
#make it such that ret_data[col][0] is min
# ~lr ret_data[col][1] is max 
# for all col in the data set
def get_max_min( dataSet):
  max_min_matrix = []
  nRow = len(dataSet)
  nCol = len(dataSet[0])-1


  for col in xrange(nCol):
    colMin = float("inf")
    colMax = float("-inf")
    for row in xrange(nRow):
      ij_val = float(dataSet[row][col])
      if ij_val > colMax:
        colMax = ij_val
      if ij_val < colMin:
        colMin = ij_val
    max_min_matrix.append([colMin, colMax])
  return max_min_matrix


def calculate_bin_sizes(max_min_matrix, n_Bins):
  binSizes = []

  #do bin size calc by
  # binSizeForNDim = (nMax - nMin) / numBins
  for col in xrange(len(max_min_matrix)):
    binSize = (max_min_matrix[col][1] - max_min_matrix[col][0]) / n_Bins
    binSizes.append(binSize)

  return binSizes 



def find_bin( value_to_bin, lower_bound, bin_size, n_Bins):
  selected_Bin = 0

  found_Bin = False

  lhs = lower_bound
  rhs = lhs + bin_size
  while not found_Bin:
    if lhs < value_to_bin and value_to_bin <= rhs:
      found_Bin = True
    elif value_to_bin <= lhs and selected_Bin == 0:
      found_Bin = True
    elif value_to_bin >= ((n_Bins * bin_size) + lower_bound):
      found_Bin = True
    else:
      lhs = rhs
      rhs += bin_size
      selected_Bin += 1

  return selected_Bin


def bin_data( dataSet, max_min_matrix, bin_sizes, n_Bins, classes):
  nuData = []
  nCol = len(dataSet[0]) -1
  #all rows
  for j in xrange(0,nCol):
    bins = []
    for n in xrange(n_Bins):
      bin_map = {}
      for cl in classes:
        bin_map[cl] = 0
      bins.append(bin_map.copy())
    # print "bins\t", bins
    for i in xrange(0,len(dataSet)):
      ij_bin = find_bin( float(dataSet[i][j]), max_min_matrix[j][0], bin_sizes[j], n_Bins)
      # print ij_bin,"\t",dataSet[i][-1]
      bins[ij_bin][dataSet[i][-1]] += 1
    nuData.append(bins[:])
    # print "nuData\t",nuData
  return nuData[:]


def count_classes_in_bins( binnedData, n_Bins):
  bin_mapping = []
  nCol = len(binnedData[0]) - 1
  for row in xrange(len(binnedData)):
    rowClass = binnedData[row][-1]
    for col in xrange(nCol):
      try:
        bin_mapping[col][binnedData[row][-1]] += 1
      except:
        print "bunnies"

  return False


def bin_prob():
  return False


def select_dims( dataIn, n_dims):
  print "getting t-test scores"
  myTScores, myPValues = metagenomicKNN.find_T_score( dataIn)
  print "found t-test scores"
  #get top ten dimensions
  #TODO: change from hard 10 dims to N dims
  theBest= metagenomicKNN.findBestNDims( n_dims, myTScores[:])

  return theBest
  


def externalRequest(testFile, trainingFile, n_Bins, classList, mEst):

  trainingData = commonFunctions.data_parser(trainingFile)
  testData = commonFunctions.data_parser(testFile)

  # del(trainingData[0])
  # del(testData[0])

  trainingData = commonFunctions.make_data_numeric(trainingData)

  testData = commonFunctions.make_data_numeric(testData)

  useDims = select_dims( trainingData, 10)

  trainingData = metagenomicKNN.pullSubsetOfData(useDims, trainingData[:])

  testData = metagenomicKNN.pullSubsetOfData(useDims, testData[:])

  #get max min for all dims
  max_min_values = get_max_min(trainingData)
  # print max_min_values

  binSizes = calculate_bin_sizes(max_min_values[:], n_Bins)
  #make upper low bound for all dims
  # print binSizes
  #bin training data & bin testing

  binnedTrainingData = bin_data(trainingData, max_min_values, binSizes, n_Bins, classList)

  correct = 0
  total = 0

  print len(binnedTrainingData)
  # print binnedTrainingData
  # for i in binnedTrainingData:
  #   for j in i:
  #     print "\t",j
  #   print "\n"
  # have old data & make new 'bin' data
  #for each elt in data
  #~~ for each col in the data 
  #~~ ~~ find bin num 
  print "\n\nThis is the bins for the test vals\n\n"
  testCols = len(testData[0]) -1
  for elt in xrange(len(testData)):
    eltBins = []
    for col in xrange(testCols):
      colBin = find_bin(float(testData[elt][col]), max_min_values[col][0], binSizes[col], n_Bins)
      eltBins.append(colBin)
    # print eltBins

    classVotes = {}
    # print "test block - Start"
    for cl in classList:
      prob = 1.0
      for dim in xrange(testCols):
        # print dim
        # print eltBins[dim]
        
        numerator = binnedTrainingData[dim][eltBins[dim]][cl] + 1
        numerator = float(numerator)
        # print "numerator\t",numerator
        denominator = sum(binnedTrainingData[dim][eltBins[dim]].values()) + mEst
        denominator = float(denominator)
        # print "denominator\t",denominator
        prob *=  numerator / denominator
      print cl,"\t",prob
      classVotes[cl] = prob
    print max(classVotes, key=classVotes.get)
    print testData[elt][-1]
    # if int(testData[elt][-1]) == int(max(classVotes, key=classVotes.get)):
    #   correct += 1
    # total += 1
    print "next\n"
    # print "End - test block\n\n"

  # print correct,"\t",total
  # print correct / (total * 1.0)
  return "$$$"


if __name__ == "__main__":
  #fruit test!
 
  trainingFile = "/Users/russkaehler/Machine Learning Spring 2012/KaehlerNaiveBayes/fruit.txt"
  testingFile = "/Users/russkaehler/Machine Learning Spring 2012/KaehlerNaiveBayes/testFruit.txt"
  classList = ['1','2','3','4']
  mEst = 1.0
  externalRequest(testingFile, trainingFile, 2, classList, mEst)










