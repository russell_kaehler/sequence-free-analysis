"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student

This is the main driver to run a Native Bayes
classifier on any given dataset"""

import naive_bayes


def bayes_driver(main_path, n_dims, n_bins, m_est, class_list):
    "" " call this function to run the driver " ""

    final_train = main_path + "/Final Train/"
    final_test = main_path + "/Final Test/"


    #input for metagenomic processing
    final_train_output = final_train + "trainingNormalized.csv"

    final_test_output = final_test + "testingNormalized.csv"

    # class_list = ["0", "1", "2"]
    #j_grass_classes = ["Transition", "Native", "Invaded"]

    c_res = naive_bayes.external_request(final_test_output, final_train_output,\
    n_bins, class_list, m_est, n_dims)
    return c_res


if __name__ == "__main__":
    machine_name = "rkaehler"

    #must have starting data set
    main_path = "/home/" + machine_name
    dims = 5
    n_bins = 3
    m_est = 0.5
    bayes_driver(main_path, dims, n_bins, m_est)
