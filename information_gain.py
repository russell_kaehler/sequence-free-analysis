import math

"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Comuter Science Grad Student
"""

def get_best_n_dims(n_dims, data_in):
    """
    ------ Get the label of the att, from the labels list, with the greatest information gain
    """
    
    #stat best val at zero 
    bestVal = 0.0
    
    #assume there is no best label
    bestAtt = None
    data_len = len(data_in)
    n_cols = len(data_in[0])

    col_gain_map = {}

    for col in xrange(n_cols):
        #calculate the information gain in the current dimension
        info_gain_in_dim = calculate_gain(data_in, col)
        
        col_gain_map[col] = info_gain_in_dim
        #if dimensional information gain is greater than or equal to current best value 
        # if dimGain >= bestVal:
        #     #set labels and value to current value
        #     bestVal = dimGain
        #     bestAtt = col

    #return the label of the best att
    return col_gain_map



def calculate_gain( data_in, col):
    """
    ------ Find information gain in the given dimension 
    """

    #generate map of current atts
    attValueMap = map_attType_counts( data_in, col)

    attSubsetEntropies = 0.0 

    #gets total entropy of current data set
    totalEntropy = get_entropy(data_in)


    for key in attValueMap.keys():
        #get vi/ |v|
        attValueProb = attValueMap[key] / sum( attValueMap.values())

        #find list of records in current data that have same value
        myAttSubset = get_same_value( data_in, key) 
        
        #sum att type entropies 
        attSubsetEntropies += attValueProb *  get_entropy(myAttSubset)

    #total info gain is totalEntropy of att - Sum of all vi entropies
    myInfoGain = totalEntropy - attSubsetEntropies
    #return infogain in dimension
    return myInfoGain
    


def get_entropy( data_in):
    """
    ------ Get entropy of given dataset assume 
    ------ last entry is class record
    """  

    #generate att count map of current data
    attValueMap = map_attType_counts( data_in, -1)

    #initialize entropy val at zero
    myEntropy = 0.0

    for counts in attValueMap.values():
        #calculate percent of this count out of the total number of counts
        myProb = counts/ sum( attValueMap.values())
        
        #calcualte entropy of of current count
        myEntropy += (-1) * myProb * math.log( myProb, 2)

        #think of count as a more genric represenation of p+ or p- where p can have more than + or - 
        #thus can find entropy of data set that has n outcomes
    return myEntropy


def map_attType_counts( data_in, col_index):
    """
    ------ Make a map of attTypes to counts
    """
    #make copy of data set
    myValueMap = {}

    for row in data_in:
        #increment count of att type if seen before
        if ( myValueMap.has_key(row[col_index])):
            myValueMap[row[col_index]] += 1.0

        #else set att type count to one 
        else:
            myValueMap[row[col_index]] = 1.0

    #return map of counts
    return myValueMap


def get_same_value( data_in, selection):
    """
    ------ Get Elts of Same value 
    """

    #list of elts that have the same value in some dimension
    mySameElts = []

    #iterate over data set
    for elt in data_in:
        #if myelt has value in dimension equal to the dimension and value we seek
        if elt[-1] == selection:
            #add such a desierable element to out list of same value elts
            mySameElts.append(elt)
    #return the wonderful list 
    return mySameElts
