"""
Copyright (c)  Russell Kaehler 2016
University of Montana 
Computer Science Grad Student
"""

import os
import shutil
import random 
import itertools

import information_gain

def getFiles(path):
    fileList = []
    for x in os.listdir(path):
        fileList.append(x)
    return fileList

def get_files_and_path(path):
    fileList = []
    for x in os.listdir(path):
        fPath = path + "/" + x
        fileList.append(fPath)
    return fileList

def copy_files(from_source, to_dest):

    return 0


def build_folder_list(folder_list):
    for path in folder_list:
        if not os.path.exists(path):
            os.makedirs(path)
    return 0


def removeExisitingData(onPath):
    eltsToRemove = getFiles(onPath)
    for elt in eltsToRemove:
        os.remove(onPath + elt)

    return 0


def clearOutputPath(exitPath):
    exitFiles = getFiles(exitPath)

    if len(exitFiles) > 0:
        rmRes = removeExisitingData(exitPath)
        if rmRes != 0:
            sys.exit(99)

    return 0


def data_parser(path_to_file):
    """ read in a file and return it as an array """
    # build list of data records
    fullData = []
    # open file read each line removing \r\n and place that into a list of
    # lines
    with open(path_to_file, 'rb') as myFile:
        # loop over lines in file
        for line in myFile:
            # read line ~lr to above
            current_line = line.replace('\r\n', '').strip()
            tempList = []
            # if line has more than one split
            if len(current_line.split(',')) > 1:
                # loop over what the split fn returns
                for value in current_line.split(','):
                    # add the values to your temp list
                    tempList.append(value)

                fullData.append(tempList[:])
    return fullData


def read_file_into_numeric_list(path):
    file_data = []
    with open(path, 'rb') as temp_file:
        for line in temp_file:
            current_line = line.replace('\r\n', '').strip()
            for value in current_line.split(','):
                # add the values to your temp list
                file_data.append(float(value))
    return file_data


def make_data_numeric(data_in):
    """ take in a data set and turn all non class
    values into floats """
    fullData = []
    nCol = len(data_in[0])
    for row in xrange(len(data_in)):
        nuRow = []
        for col in xrange(nCol):
            if col != (nCol - 1):
                nuRow.append(float(data_in[row][col]))
            else:
                nuRow.append(data_in[row][col])
        fullData.append(nuRow[:])
    return fullData


def pull_subset_of_data_keep_class_vect_old(winning_dims, data_in):
    """ take a list of column indicies and reduce your data down to that """
    data_subset = []
    key_list = winning_dims.keys()
    n_keys = len(key_list)
    n_rows = len(data_in)
    for row in xrange(0, n_rows):
        nu_row = []
        for x in xrange(0, n_keys):
            col = winning_dims[key_list[x]]
            val = data_in[row][col]
            nu_row.append(val)
        nu_row.append(data_in[row][-1])
        data_subset.append(nu_row)
    return data_subset

def pull_subset_of_data_keep_class_vect(winning_dims, data_in):
    """ take a dict of indicies {index: score} and reduce your data down to 
    only include the indicies from the winning_dims map """
    data_subset = []
    # key_list = winning_dims.keys()
    n_rows = len(data_in)
    for row in xrange(0, n_rows):
        nu_row = []
        for col in winning_dims:
            val = data_in[row][col]
            nu_row.append(val)
        nu_row.append(data_in[row][-1])
        data_subset.append(nu_row)
    return data_subset


def pull_subset_of_data_change_class_to_numbers(winning_dims, data_in, class_map):
    """ NOT READY; copied from pull_subset_of_data_keep_class_vect """
    data_subset = []
    key_list = winning_dims.keys()
    n_keys = len(key_list)
    n_rows = len(data_in)
    for x in xrange(n_keys):
        col = winning_dims[key_list[x]]
        for row in xrange(n_rows):
            if x == 0:
                data_subset.append([])
            val = data_in[row][col]
            data_subset[row].append(val)
    return data_subset

def mix_files(data_start_path, class_options):
    """ number of files nees to be even... probably"""
    # get files
    ini_file_list = getFiles(data_start_path)
    split_classes = {}
    # orders = itertools.permutations(ini_file_list)
    # organize sort
    for item in ini_file_list:
        for opt in class_options:
            if opt in item:
                try:
                    split_classes[opt].append(item)
                except:
                    split_classes[opt] = [item]
    # files will be a set of permeutations
    # do count of lines
    rand_orders = {}
    for opt in class_options:
        # print opt
        rand_orders[opt] = []
        orders = itertools.permutations(split_classes[opt])
        for x in orders:
            rand_orders[opt].append(list(x))

    test_files = []
    training_files = []
    for opt in class_options:
        list_len = len(rand_orders[opt])

        for index in xrange(list_len):
            arangement = rand_orders[opt][index]
            # print arangement
            test_half = arangement[:len(arangement) / 2]
            train_half = arangement[len(arangement) / 2:]
            try:
                for elt in test_half:
                    test_files[index].append(elt)
                for alt in train_half:
                    training_files[index].append(alt)
            except:
                test_files.append([])
                training_files.append([])
                for elt in test_half:
                    test_files[index].append(elt)
                for alt in train_half:
                    training_files[index].append(alt)
    random.shuffle(test_files)
    random.shuffle(training_files)
    return test_files, training_files


def loocv_mix(data_start_path):
    ini_file_list = getFiles(data_start_path)
    random.shuffle(ini_file_list)
    test_order = []
    training_order = []
    for item in ini_file_list:
        test_order.append([item])
        alt_list = ini_file_list[:]
        alt_list.remove(item)
        training_order.append(alt_list)
    return test_order, training_order


def get_best_n_dims_from_information_gain( n_dims, data_in):
    best_n_dims = information_gain.get_best_n_dims(n_dims, data_in)
    return best_n_dims


def pull_random_n_dims( n_dims, data_in):
    data_size = len(data_in[0])
    indices = random.sample( xrange(data_size), n_dims)
    selected_dims = {}
    for x in indices:
        selected_dims[x] = 0.90
    return selected_dims.copy()

def find_best_n_dims_from_t_test_results( n_dims, t_results):
    # total_dims = n_dims + 1
    # print t_results
    dim_results = sorted(range(len(t_results)), key=lambda i:t_results[i])
    # print dim_results
    # print len(dim_results)
    alt_results = dim_results[::-1]
    # print alt_results
    alt_results = alt_results[:n_dims]
    # print alt_results
    alt_results = sorted(alt_results)
    # print alt_results
    # print len(alt_results)
    return alt_results[:]


def export_data_to_file(data_in, export_path):
    """ needs to be implemented so LDA can run... also, get LDA working"""
    with open(export_path, 'a') as outFile:
        for q in range(0,len(data_in)):
            tempLine = data_in[q]
            tempLen = len(tempLine)
            for r in range(0, tempLen):
                val = str(tempLine[r])
                if (r != tempLen-1):
                    outFile.write( val + ",")
                else:
                    outFile.write( val + "\n")


    print 1
