"""
Copyright (c)  Russell Kaehler 2016
University of Montana
Computer Science Grad Student

This is the script to run and evaluate the crossfold tests for
the naive bayes tests of the metagenomic data
"""
import subprocess
import time
import random

import setup_folders
import commonFunctions

def build_folders(write_path, n_trials):

    data_write_paths = []
    for i in xrange(n_trials):
        base_path = "/" + write_path + str(i)
        setup_folders.hpc_build(base_path)
        data_write_paths.append(data_write_paths)

    return data_write_paths


def write_out_msub_request(job_folder, data_read_path, write_path, mer_size, n_dims, trial_number):

    trial_node = trial_number
    if trial_number == 6:
        while trial_node == 6:
            trial_node = random.randint(0,19)

    request_title = "#PBS -N job_request-" + str(trial_number)
    msub_list = [
        "#!/bin/sh",
        "#PBS -l nodes=n"+str(int(trial_node) % 19)+":ppn=1",
        "#PBS -j oe",
        request_title,
        "#PBS -d /home/um/rkaehler/",
        "#PBS -S /bin/bash",
        "#PBS -m e",
        "#PBS -l walltime=20:00:00:00",
        "hostname #linux command",
        # "module load python/2.7.11"
    ]

    job_write_path = write_path + str(trial_number)    
    # python_request = "python ~/sequence-free-analysis/bayes_crossfold.py --read_path \""+data_read_path+ "\" --write_path \""+ job_write_path +"\" --mer_size "+ str(mer_size) +" --n_dims "+  str(n_dims)+"\n"
    python_request = "python ~/sequence-free-analysis/multi_ml_crossfold.py --read_path \""+data_read_path+ "\" --write_path \""+ job_write_path +"\" --mer_size "+ str(mer_size) +" --n_dims "+  str(n_dims)+"\n"

    job_name = job_folder + "/job_request-" + str(trial_number)
    with open(job_name, "w") as job_file:
        for line in msub_list:
            job_file.write(line + "\n")
        job_file.write(python_request)



def launch_jobs(job_folder):
    jobs = commonFunctions.get_files_and_path(job_folder)

    for job in jobs:
        subprocess.Popen(["msub", job])
        time.sleep(3)


def prep_jobs(n_jobs, read_path, write_path, mer_size, n_dims, job_folder):

    data_files = build_folders(write_path, n_jobs)
    commonFunctions.clearOutputPath(job_folder +"/")
    for i in xrange(n_jobs):
        write_out_msub_request(job_folder, read_path, write_path, mer_size, n_dims, i)

    launch_jobs(job_folder)
    return 0


if __name__ == "__main__":

    read_path = "data1/rkaehler"

    write_path = "data1/rkaehler/Temps"

    moab_jobs = "/home/um/rkaehler/moab"

    mer_size = 5

    n_dims = 10

    n_jobs = 20

    write_path += "/"+str(mer_size)+"Mer"+"_"+str(n_dims)+"Dim-"

    prep_jobs(n_jobs, read_path, write_path, mer_size, n_dims, moab_jobs)
