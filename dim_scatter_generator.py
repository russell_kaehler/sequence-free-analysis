"""
Copyright (c)  Russell Kaehler 2017
University of Montana
Computer Science Grad Student

This script parses log files and produces a scatter plot of 
frequency of dimension selection vs count of mers in that same dimension
"""
import matplotlib.pyplot as plt

import commonFunctions
import bar_plot_of_dimensions
import index_to_mer


def get_dim_count(read_path, write_path, end_name):
    log_files = commonFunctions.get_files_and_path(read_path)
    mer_size_to_selection = {}
    num_trials = {}
    num_dims_used = {}
    total_possible = 0
    mer_size_string =""
    final_order = []
    # print log_files
    for log in log_files[:]:
        # print log
        rhs = log.split("Using_")[1]
        mer_size_string = rhs.split("_")[0]
        # print mer_size
        if mer_size_string not in mer_size_to_selection.keys():
            mer_size_to_selection[mer_size_string] = {}
            num_trials[mer_size_string] = 1
            num_dims_used[mer_size_string] = -1
        else:
            num_trials[mer_size_string] += 1
        dim_count_from_log, n_dims, total_in_run = bar_plot_of_dimensions.parse_log_format(log)
        total_possible += total_in_run
        if num_dims_used[mer_size_string] == -1:
            num_dims_used[mer_size_string] = n_dims
        elif num_dims_used[mer_size_string] != n_dims:
            sys.exit(1)
        
        for key in dim_count_from_log.keys():
            try:
                mer_size_to_selection[mer_size_string][key] += dim_count_from_log[key]
            except:
                mer_size_to_selection[mer_size_string][key] = dim_count_from_log[key]
        # print dim_count_from_log
    
    mer_size = int(mer_size_string.split("-")[0])
    for key in mer_size_to_selection.keys():
        print "there were\t",num_trials[key], "\truns of\t",key
        ordered_tuples = [ (v,k) for k,v in mer_size_to_selection[key].iteritems() ]
        ordered_tuples.sort(reverse=True)
        ordered_keys = ["max"]
        ordered_values = [total_possible]
        #is limit needed here?
        limit = num_dims_used[key]
        for v,k in ordered_tuples:
            mer_str = index_to_mer.external_request(int(k), int(mer_size))
            final_order.append([k,v])
            # print "%d: %d" % (int(k),int(v))
            print str(k)+","+mer_str+","+str(v)
            if int(v) > 1:
                ordered_keys.append(int(k))
                ordered_values.append(int(v))

        print ordered_values

    # return mer_size_to_selection.copy()
    return final_order



if __name__ == "__main__":
    plot_out_path = "/home/um/rkaehler/plots"
    test_folder = "HG_eightMer_fiveDim"
    k_size = "8"
    n_dims = "5"
    log_dest = "/home/um/rkaehler/tempLogs/nb" + k_size + "mer" + n_dims + "dimHG"
    temp_path = "/data1/rkaehler/Temps/" + test_folder
    selected_substring = "BC_on_Human_Gut_Using_" + k_size + "-mers_on_" + n_dims + "-dims"

    plt_name = k_size + "mer_" + n_dims + "dim_scatter_cnt_vs_selection_HG"

    dim_map = get_dim_count(log_dest + "/"+ test_folder, plot_out_path, selected_substring)

    total_counts = {}

    final_map = {}

    dim_list = []
    print dim_map

    dim_selection = {}
    dim_to_freq = {}

    for dim in dim_map:
        dim_list.append(dim[0])
        dim_selection[dim[0]] = 0
        dim_to_freq[dim[0]] = dim[1]

    trial_list = commonFunctions.get_files_and_path(temp_path)

    # print trial_list

    print dim_list
    total_count = 0
    for trial in trial_list:
        nsv_records = commonFunctions.get_files_and_path(trial +"/NSV Train")
        for nsv_path in nsv_records:
            my_nsv = commonFunctions.read_file_into_numeric_list(nsv_path)
            for elt in xrange(len(my_nsv)):
                if elt in dim_list:
                    dim_selection[elt] += my_nsv[elt]
                total_count += my_nsv[elt]
                # dim_selection[dim] += my_nsv[dim]
            # print trial
            # print nsv_path
            # print len(my_nsv)
            # print "\n\n"
    print dim_selection

    print "\n\n"

    # print total_counts

    x_vals = []
    y_vals = []
    for dim in dim_list:
        final_map[dim] = [ dim_selection[dim], dim_to_freq[dim]]
        x_vals.append(dim_selection[dim])
        y_vals.append(dim_to_freq[dim])



    print "this is the final map"
    print final_map

    print "making plot"
    my_plt = plt.figure()
    plt.plot(x_vals, y_vals, "o")

    plt.ylabel('Frequency of Selection')
    plt.xlabel('Raw Count of K-Mer in Dimension')
    # plt.show()

    plt.savefig(plot_out_path + "/" + plt_name + ".png")
    




